/*
import { resolvePath } from './utils.helper';
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { GetDeviceModel } from './devices.helper'
import { resolvePath } from './utils.helper'

export const SOURCE_KEY = '/src.json'
export const BUILD_KEY = '/build.json'
export const BSP_KEY = 'bsp'

export const MapBuildToVersion = (buildJson) => ({
  version: buildJson.commit
})

export const MapSrcToVersion = (srcJson) => ({
  version: srcJson.docker_digest
})

export const MapBuildToSrc = (buildJson) => ({
  docker_digest: buildJson.commit,
  docker_name: buildJson.project
})

export const FindPlatform = (state = {}, platformName, excludes = []) => {
  return Object.keys(state).reduce((acc, key) => {
    const start = key.split('/')[0]
    if (key.indexOf(`${platformName}/`) >= 0 && excludes.indexOf(start) === -1) {
      acc[key] = state[key]
    }
    return acc
  }, {})
}

export const RemovePlatform = (state = {}, platformName) => {
  return Object.keys(state).reduce((acc, key) => {
    if (key.indexOf(platformName) >= 0) {
      delete acc[key]
    } else {
      acc[key] = state[key]
    }
    return acc
  }, {})
}

export const GetPlatforms = (state) => {
  return Object.keys(state).reduce((acc, key) => {
    if (key.indexOf(SOURCE_KEY) >= 0 && key.indexOf(BSP_KEY) < 0) {
      const platform = key.replace(SOURCE_KEY, '')
      acc[platform] = state[key]
    }
    if (key.indexOf(BUILD_KEY) >= 0 && key.indexOf(BSP_KEY) >= 0) {
      acc[BSP_KEY] = MapBuildToSrc(state[key])
    }
    return acc
  }, {})
}

export const GetPlatformsWithUpdate = (device, source) => {
  return Object.keys(source).reduce((acc, key) => {
    const isNew = !device[key] ||
      (device[key].docker_digest !== source[key].docker_digest)

    if (isNew) {
      acc.push(key)
    }
    return acc
  }, [])
}

export const GetPosibleUpdates = (sources, destinationState, device) => {
  const devicePlatforms = GetPlatforms(destinationState)
  const model = GetDeviceModel(device)
  const sourceState = resolvePath(sources, `${model}.state`, {})
  const sourcePlatforms = GetPlatforms(sourceState)
  const platformsWithUpdate = GetPlatformsWithUpdate(devicePlatforms, sourcePlatforms)

  return {
    platformsWithUpdate,
    sourcePlatforms,
    sourceState
  }
}
