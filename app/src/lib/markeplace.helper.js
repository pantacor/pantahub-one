/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { resolvePath, DigestMessage } from './utils.helper'
import { FindPlatform, SOURCE_KEY } from './state.helper'

function isObject (val) {
  return val instanceof Object
}

export const USERMETA_MARKETPLACE_KEY = 'one.marketplace.external.sources'
export const APP_CONFIG_JSON_KEY = 'v0alpha.app.manifest.json'

export const GetExtenalMarketSources = (deviceData) => {
  const userMeta = resolvePath(deviceData, 'device.user-meta')
  return JSON.parse(userMeta[USERMETA_MARKETPLACE_KEY] || '[]')
}

export const GetMarketSources = (deviceData) => {
  const sources = process.env.REACT_APP_OFFICIAL_MARKETPLACE_SOURCE &&
    process.env.REACT_APP_OFFICIAL_MARKETPLACE_SOURCE !== ''
    ? [process.env.REACT_APP_OFFICIAL_MARKETPLACE_SOURCE]
    : []

  const externals = GetExtenalMarketSources(deviceData)

  return [
    ...new Set([...sources, ...externals])
  ]
}

export const GetAppsConfFromState = (state) => {
  return Object.keys(state).reduce((acc, key) => {
    if (key.indexOf(APP_CONFIG_JSON_KEY) >= 0) {
      const appName = key.replace(`/${APP_CONFIG_JSON_KEY}`, '')
      acc[appName] = state[key]
    }
    return acc
  }, {})
}

export const GetAppsNamesFromState = (state, deviceId) => {
  return Object.keys(state).reduce((acc, key) => {
    if (key.indexOf(APP_CONFIG_JSON_KEY) >= 0) {
      const appName = key.replace(`/${APP_CONFIG_JSON_KEY}`, '')
      acc.push({
        source: deviceId,
        key: appName,
        logo: state[key].logo,
        title: state[key].title,
        state: FindPlatform(state, appName)
      })
    }
    return acc
  }, [])
}

export const NormalizeJsonToMeta = (json, prefix) => {
  return Object.keys(json).reduce((acc, key) => {
    const newKey = prefix ? `${prefix}.${key}` : key
    if (isObject(json[key])) {
      acc = {
        ...acc,
        ...NormalizeJsonToMeta(json[key], newKey)
      }
    } else if (Array.isArray(json[key])) {
      acc = {
        ...acc,
        ...NormalizeJsonToMeta(Object.assign({}, json[key], newKey))
      }
    } else {
      acc[newKey] = json[key].toString()
    }

    return acc
  }, {})
}

export const GetAppVersion = async (state, platform) => {
  const platformState = FindPlatform(state, platform, ['_config'])
  const digestMessage = DigestMessage(JSON.stringify(platformState))
  if (digestMessage) {
    return digestMessage
  }

  const dockerDigest = (state[`${platform}${SOURCE_KEY}`] || {})['docker_digest']
  const manifestVersion = state[`${platform}/${APP_CONFIG_JSON_KEY}`].version
  return `${manifestVersion}-${dockerDigest}`
}

export const UpdateConfigOnState = (state, app, config, data) => {
  return {
    ...state,
    [`_config/${app}${config.file}`]: data
  }
}
