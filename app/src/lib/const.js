/*
import { SUPPORTED_MODELS } from './const';
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const SECURITY_OPTIONS = {
  OPEN: 'Open',
  SECURE: 'Secure',
  KIDS: 'Kids'
}

export const CURRENT_STEP_NOTIFICATIONS = {
  NONE: '',
  LOADING: 'Loading',
  SUCCESS: 'Success',
  WARNING: 'Warning',
  ERROR: 'Error'
}

export const MODELS = {
  RPI3: 'rpi3',
  RPI4: 'rpi4',
  RPI64: 'rpi64'
}

export const SUPPORTED_BOARDS_MODELS = [
  'Raspberry Pi 4 Model B',
  'Raspberry Pi 3 Model B',
  'Wallystech DR40X9',
  'Broadcom-v8A',
  'Bananapi BPI-R64',
  'NVIDIA Jetson Nano Developer Kit'
].map((t) => t.toLowerCase())

export const SUPPORTED_ARCH = [
  'arm/32/EL',
  'arm/64/EL',
  'aarch64/64/EL',
  'unknown/64/EL'
].map((t) => t.toLowerCase())

export const SOURCE_DEVICE_ID = process.env.REACT_APP_SOURCE_DEVICE_ID
export const CLOUDFLARE_DOCKER_NAME = 'registry.gitlab.com/pantacor/pv-platforms/cloudflare-dns'
export const AWCONNECT_DOCKER_NAME = 'registry.gitlab.com/pantacor/pv-platforms/wifi-connect'
export const AVAHI_DOCKER_NAME = 'registry.gitlab.com/pantacor/pv-platforms/pv-avahi'

export const HTTPD_USER_USER_META_KEY = 'pvr-sdk.httpd.user'
export const HTTPD_PASS_USER_META_KEY = 'pvr-sdk.httpd.secret'
export const HTTPD_USER = 'admin'
export const HTTPD_PASS = 'cloudflare'

export const SECURITY_OPTION_USER_META_KEY = 'cloudflared.upstream'
export const CLOUDFARE_OPEN_URL = 'https://cloudflare-dns.com/dns-query'
export const CLOUDFARE_SECURE_URL = 'https://security.cloudflare-dns.com/dns-query'
export const CLOUDFARE_KIDS_URL = 'https://family.cloudflare-dns.com/dns-query'

export const DEFAULT_DEVICE_DNS_CONF_VALUE = '304a06dbba29eaf37588e8ff71dcb2df33bbb3a6e42ea9e65fe9cc43c4674054'
export const DEFAULT_DEVICE_HOSTNAME_VALUE = '2c8b08da5ce60398e1f19af0e5dccc744df274b826abe585eaba68c525434806'
export const DEFAULT_WIFI_SSID = 'Pantacor-One'
export const DEFAULT_WIFI_PSK = 'pantahubone'
export const DEFAULT_WIFI_SECURITY = 'WAP2'
export const DEFAULT_WIFI_RADIO = '2.4Ghz'
