/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const LOCALSTORAGE_KEY = 'localdevices'

export const GetLocalDeviceImageUrl = (deviceIp = 'one.local', deviceId) => {
  return !deviceId
    ? `http://${deviceIp}:8280/pantacor64.png`
    : `http://${deviceIp}:8280/pantacor64-${deviceId}.png`
}

const readData = () =>
  JSON.parse(window.localStorage.getItem(LOCALSTORAGE_KEY) || `{}`)

const saveData = (data) =>
  window.localStorage.setItem(LOCALSTORAGE_KEY, JSON.stringify(data))

const catchAndLog = (cb, d) => {
  try {
    return cb()
  } catch (e) {
    console.info(e)
    return d
  }
}

export const AddDevice = (id, device) => catchAndLog(() => {
  const devices = readData()
  saveData({
    ...devices,
    [id]: device
  })
})

export const RemoveDevice = (id) => catchAndLog(() => {
  const devices = readData()
  devices[id] = undefined
  saveData(devices)
})

export const GetDevices = () => catchAndLog(readData, {})

export const GetDevice = (id) => catchAndLog(() => {
  return readData()[id]
})

export const DeviceCount = () => catchAndLog(() => {
  return Object.keys(readData()).length
})
