/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { resolvePath } from './utils.helper'
import { MODELS } from './const'

export const EXPANDED_TABS = {
  NONE: 'none',
  SETTINGS_MANAGER: 'settings_manager',
  GAMES_FILTER: 'game_filter',
  INTERNET_FILTER: 'internet_filters',
  SETTINGS: 'settings'
}

export const togleStatus = (newStatus, oldStatus) =>
  newStatus === oldStatus ? EXPANDED_TABS.NONE : newStatus

export const GetDeviceModel = (device) => {
  const metaData = resolvePath(device, 'device-meta', {})
  const model = (metaData['pantavisor.dtmodel'] || '').toLowerCase()
  const arch = (metaData['pantavisor.arch'] || '').toLowerCase()

  if (model.indexOf('raspberry pi 3') >= 0 && arch === 'arm/32/EL') {
    return MODELS.RPI3
  }

  if (model.indexOf('raspberry pi 4') >= 0 && arch === 'arm/32/EL') {
    return MODELS.RPI4
  }

  if (model.indexOf('raspberry pi') >= 0 && arch === 'aarch64/64/EL') {
    return MODELS.RPI64
  }

  return model
}

export const GetSourceIdByModel = (model) => {
  switch (model) {
    case MODELS.RPI3:
      return process.env.REACT_APP_SOURCE_DEVICE_RPI3
    case MODELS.RPI4:
      return process.env.REACT_APP_SOURCE_DEVICE_RPI4
    case MODELS.RPI64:
      return process.env.REACT_APP_SOURCE_DEVICE_RPI64
    default:
      return process.env.REACT_APP_SOURCE_DEVICE_RPI3
  }
}
