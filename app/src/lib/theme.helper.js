/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { createMuiTheme } from '@material-ui/core/styles'
import { makeStyles } from '@material-ui/core'
import ThemeProvider from '@material-ui/styles/ThemeProvider'

const primaryColor = '#15162D'
const successColor = 'rgb(140,194,72)'
const warningColor = 'rgb(255,150,6)'
const dangerColor = 'rgb(220,65,47)'
const disabledColor = 'rgb(189,189,189)'
const grayIconColor = 'rgb(190,190,190)'

const backupFontFamily = ['Montserrat', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue', 'sans-serif']
const mediumFontFamily = ['Montserrat', ...backupFontFamily]
const semiboldFontFamily = ['Montserrat', ...backupFontFamily]

const sfptMedium = {
  fontFamily: 'Montserrat',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 400
}

const sfptSemibold = {
  fontFamily: 'Montserrat',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  fontWeight: 600
}

export const theme = createMuiTheme({
  typography: {
    fontFamily: mediumFontFamily
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [sfptMedium, sfptSemibold]
      }
    }
  },
  palette: {
    primary: {
      main: '#15162D',
      contrastText: '#fff'
    },
    secondary: {
      main: 'rgb(140,194,72)',
      contrastText: '#fff'
    }
  },
  status: {
    danger: 'orange'
  },
  shadows: ['none']
})

export const ThemeProviderLoaded = ({ children }) => (
  <ThemeProvider theme={theme}>
    {children}
  </ThemeProvider>
)

export const useThemeStyles = makeStyles(theme => ({
  icon: {
    marginRight: theme.spacing(2)
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 6)
  },
  heroButtons: {
    marginTop: theme.spacing(8)
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8)
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column'
  },
  cardMedia: {
    paddingTop: '56.25%' // 16:9
  },
  cardContent: {
    flexGrow: 1
  },
  linearProgress: {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2)
    }
  },
  flex: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    '& > *': {
      margin: theme.spacing(1)
    }
  },
  vertialSpace: {
    marginBottom: theme.spacing(2)
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6)
  },
  button: {
    'text-transform': 'capitalize !important',
    'font-family': mediumFontFamily,
    'font-size': '15px !important',
    'line-height': '1.5 !important',
    'box-shadow': 'initial !important'
  },
  primary: {
    backgroundColor: `${primaryColor} !important`,
    color: 'white !important'
  },
  success: {
    backgroundColor: `${successColor} !important`,
    color: 'white !important'
  },
  warning: {
    backgroundColor: `${warningColor} !important`,
    color: 'white !important'
  },
  danger: {
    backgroundColor: `${dangerColor} !important`,
    color: 'white !important'
  },
  disabled: {
    backgroundColor: `${disabledColor} !important`,
    color: 'white !important'
  },
  colorDisabled: {
    opacity: 0.3
  },
  successOption: {
    backgroundColor: `${successColor} !important`,
    color: 'white !important'
  },
  gamesButton: {
    backgroundColor: `#871885 !important`,
    color: `white !important`
  },
  warningOption: {
    backgroundColor: `${warningColor} !important`,
    color: 'white !important'
  },
  dangerOption: {
    backgroundColor: `${dangerColor} !important`,
    color: 'white !important'
  },
  notSelected: {
    opacity: 0.45,
    '&:hover': {
      opacity: 1.0
    }
  },
  selected: {
    opacity: 1
  },
  deviceContainer: {
    margin: theme.spacing(0)
  },
  deviceHeaderLarge: {
    padding: theme.spacing(1, 2)
  },
  deviceHeaderSmall: {
    padding: theme.spacing(1, 1)
  },
  deviceHeaderEnabled: {
    backgroundColor: 'rgb(240,240,240)',
    color: 'rgb(28,21,101)'
  },
  deviceHeaderDisabled: {
    backgroundColor: 'rgb(211,211,211)',
    color: 'white'
  },
  deviceHeaderLoadingWrapper: {
    width: '24px',
    height: '24px',
    padding: '0 !important',
    margin: '0 !important'
  },
  deviceHeaderLoading: {
    width: '24px',
    height: '24px'
  },
  deviceEditableIcons: {
    color: grayIconColor,
    'font-size': '1rem !important'
  },
  deviceEditableButtons: {
    color: grayIconColor,
    'font-size': '0.8rem !important',
    'text-transform': 'lowercase !important',
    'font-family': mediumFontFamily,
    'min-width': 'initial !important',
    padding: theme.spacing(0, 0.5)
  },
  deviceNetworkPasswordVisibleButton: {
    position: 'absolute !important',
    right: 0,
    bottom: 0
  },
  deviceName: {
    'font-family': semiboldFontFamily,
    'font-size': '16px',
    padding: theme.spacing(1)
  },
  deviceBody: {
    backgroundColor: 'rgb(248,248,248)',
    position: 'relative'
  },
  deviceBodyModal: {
    position: 'absolute',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    justifyItems: 'center',
    alignContent: 'center',
    alignItems: 'center',
    left: 0,
    'z-index': 100000,
    backgroundColor: '#f0f0f0 !important',
    opacity: 0.95,
    width: '100%',
    height: '100%',
    padding: '1rem 0rem !important',
    margin: '0 !important',
    'font-family': semiboldFontFamily,
    'font-size': '16px'
  },
  deviceBodyLoadingWrapper: {
    'border-radius': '0 !important'
  },
  deviceSuccessNotification: {
    color: `${successColor} !important`
  },
  deviceWarningNotification: {
    color: `${warningColor} !important`
  },
  deviceBodyFormLarge: {
    color: grayIconColor
  },
  deviceBodyForm: {
    color: grayIconColor
  },
  deviceBodyFormSmall: {
    color: grayIconColor
  },
  deviceBodyFormBlock: {
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1)
  },
  deviceBodyFormBlockInputItem: {
    position: 'relative !important'
  },
  deviceBodyFormIcon: {
    color: grayIconColor
  },
  deviceBodyButtonsBlock: {
    paddingTop: theme.spacing(2),
    paddingBottom: theme.spacing(2)
  },
  fullWidth: {
    width: '100%'
  },
  textLarge: {
    fontSize: '1em'
  }
}))
