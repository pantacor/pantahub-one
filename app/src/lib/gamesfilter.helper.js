/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { ObjectToArray } from './utils.helper'

import MinecraftLogo from '../assets/images/games/MinecraftLogo.svg'
import FortniteLogo from '../assets/images/games/FortniteLogo.svg'

export const BuildDnsExtraConf = (selectedFilters) => {
  let filters = '# Games Filters\n'
  selectedFilters.forEach(element => {
    if (element) {
      filters += `
        # filter for ${GameFiltersMap[element].name}
        ${GameFiltersMap[element].filter}
      `
    }
  })

  return filters
}

export const GameFiltersMap = {
  'fornite': {
    title: 'Fortnite',
    name: 'fornite',
    logo: FortniteLogo,
    filter: `
      address=/epicgames.com/0.0.0.0
    `
  },
  'minecraft': {
    title: 'Minecraft',
    name: 'minecraft',
    logo: MinecraftLogo,
    comingsoon: true,
    filter: `
      address=/minecraft.com/0.0.0.0
    `
  }
}

export const GameFilters = ObjectToArray(GameFiltersMap)
