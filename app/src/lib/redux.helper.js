/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

const defaultSuccessCb = (state, action, successPrefix) => {
  return action.payload
}

export function buildBasicActions (types = {}, actionPrefix = '') {
  return {
    success: (payload) => ({
      type: types[`${actionPrefix}_SUCCESS`],
      payload
    }),
    failure: (error) => ({
      type: types[`${actionPrefix}_FAILURE`],
      error
    }),
    inProgress: () => ({
      type: types[`${actionPrefix}_INPROGRESS`]
    })
  }
}

export function buildIndexedActions (types = {}, actionPrefix = '') {
  return {
    success: (id, payload) => ({
      type: types[`${actionPrefix}_SUCCESS`],
      id,
      payload
    }),
    failure: (id, error) => ({
      type: types[`${actionPrefix}_FAILURE`],
      id,
      error
    }),
    inProgress: (id) => ({
      type: types[`${actionPrefix}_INPROGRESS`],
      id
    })
  }
}

export function buildBasicReducers (STATES, types, typePrefix, successPrefix, failurePrefix = 'error', successCb = defaultSuccessCb) {
  const INPROGRESS = types[`${typePrefix}_INPROGRESS`]
  const FAILURE = types[`${typePrefix}_FAILURE`]
  const SUCCESS = types[`${typePrefix}_SUCCESS`]
  return {
    [INPROGRESS]: (state, action) => ({
      ...state,
      status: STATES.IN_PROGRESS
    }),
    [FAILURE]: (state, action) => ({
      ...state,
      [`${failurePrefix}`]: action.error,
      status: STATES.FAILURE
    }),
    [SUCCESS]: (state, action) => {
      return !successPrefix
        ? {
          ...state,
          ...successCb(state, action, successPrefix),
          status: STATES.SUCCESS
        } : {
          ...state,
          [successPrefix]: successCb(state, action, successPrefix),
          status: STATES.SUCCESS
        }
    }
  }
}
