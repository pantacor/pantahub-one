/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useEffect, useState } from 'react'
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'
import useMediaQuery from '@material-ui/core/useMediaQuery'

import Loading from '../../atoms/Loading/Loading'
import Device from '../Device/Device'
import { STATUS as UserStatus } from '../../../store/auth/reducer'
import { STATUS as DevicesStatus } from '../../../store/devices/reducer'

import { getDevices } from '../../../store/devices/actions'
import { useInterval } from '../../../hooks/useInterval'
import { orderDevices } from '../../../store/devices/platformSetup'
import { ObjectToArray } from '../../../lib/utils.helper'
import Typography from '@material-ui/core/Typography'
import { useThemeStyles } from '../../../lib/theme.helper'
import { MODELS } from '../../../lib/const'
import { getBaseDevice } from '../../../store/base-system/actions'

function Devices ({ user, devices, getDevices, getBaseDevice }) {
  const classes = useThemeStyles()
  // eslint-disable-next-line no-unused-vars
  const [state, setState] = useState({
    loading: true,
    polling: true
  })
  const { removeInterval, startInterval } = useInterval(getDevices, false)
  const baseInterval = useInterval(() => {
    getBaseDevice(MODELS.RPI3)
    getBaseDevice(MODELS.RPI4)
  }, false, 20000)

  const biggerThanMediumScreen = useMediaQuery('(min-width:960px)') // Material UI medium size

  const togglePolling = (value) => {
    setState((state) => ({ ...state, polling: value === null ? !state.polling : value }))
  }

  useEffect(() => {
    if (!state.polling) {
      removeInterval()
      baseInterval.removeInterval()
      return
    }

    const readyToPoll =
      user.status === UserStatus.SUCCESS &&
      (
        devices.status === null ||
        devices.status === DevicesStatus.SUCCESS ||
        devices.status === DevicesStatus.FAILURE ||
        devices.status === DevicesStatus.DEPLOY.SUCCESS ||
        devices.status === DevicesStatus.DEPLOY.FAILURE
      )

    if (readyToPoll) {
      startInterval()
      baseInterval.startInterval()
      return
    }

    if (user.status === UserStatus.SUCCESS &&
      devices.status === DevicesStatus.DEPLOY.IN_PROGRESS
    ) {
      removeInterval()
      baseInterval.removeInterval()
    }
  }, [
    state,
    state.polling,
    devices,
    devices.status,
    getDevices,
    user,
    user.status,
    startInterval,
    removeInterval,
    baseInterval
  ])

  useEffect(() => {
    if (devices.status === DevicesStatus.IN_PROGRESS ||
        devices.status === DevicesStatus.CLONING.IN_PROGRESS ||
        devices.status === DevicesStatus.DEPLOY.IN_PROGRESS ||
        devices.status === DevicesStatus.WAITING_DEPLOY) {
      setState((state) => ({ ...state, loading: true }))
    } else {
      setState((state) => ({ ...state, loading: false }))
    }
  }, [devices, devices.status])

  if (devices.list.total <= 0) {
    return (<div className={classes.heroContent}>
      <Container maxWidth='md'>
        <Grid container direction="column" justify='center' alignItems="center">
          <Grid item>
            <Loading />
          </Grid>
          <Grid>
            <Typography variant="body1" >
              Loading devices from Pantahub
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </div>)
  }

  return (
    <div className={classes.heroContent}>
      <Container maxWidth='md'>
        <React.Fragment>
          {orderDevices(ObjectToArray(devices.list.items)).map((deviceData, i) => (
            <Device
              key={deviceData.device.id}
              togglePolling={togglePolling}
              deviceData={deviceData}
              classes={classes}
              biggerThanMediumScreen={biggerThanMediumScreen}/>
          ))}
        </React.Fragment>
        {devices.list.unsupportedDevicesCount > 0 && (
          <Grid container spacing={2} justify='center'>
            <Grid item xs={12} className={classes.deviceContainer}>
              <Grid
                container
                alignItems='center'
                className={classes.deviceHeaderDisabled + ' ' + (biggerThanMediumScreen ? classes.deviceHeaderLarge : classes.deviceHeaderSmall)}>
                <Grid
                  container
                  item
                  md={6}
                  xs={12}
                  justify='flex-start'
                  alignItems='center'>
                  <Grid item className={`${classes.deviceName}`}>
                    You have {devices.list.unsupportedDevicesCount} unsupported devices
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        )}
      </Container>
    </div>
  )
}

export default connect(
  (state) => ({
    devices: state.devices,
    user: state.auth
  }),
  {
    getDevices,
    getBaseDevice
  }
)(Devices)
