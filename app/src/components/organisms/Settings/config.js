/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { useState } from 'react'
import * as AppsConfig from '../SettingsList/Apps/config'
import * as UpdatesConfig from '../SettingsList/UpdateManager/config'
import { Track } from '../../../lib/analytics.helper'

export const DEFAULT_APP = UpdatesConfig.key

// Manage application list all apps are lazy in order to make bundle smaller
export const APPS = {
  [UpdatesConfig.key]: { ...UpdatesConfig },
  [AppsConfig.key]: { ...AppsConfig }
}

export const APP_LIST = Object.keys(APPS).reduce((list, key) => {
  list.push(APPS[key])
  return list
}, [])

export function useTabs (defaultTab = DEFAULT_APP) {
  const [tab, setTab] = useState(defaultTab)

  const selectApp = (clickedApp) => (event) => {
    event.preventDefault()
    Track(clickedApp, clickedApp)
    setTab(clickedApp)
  }

  return {
    tab,
    selectApp
  }
}
