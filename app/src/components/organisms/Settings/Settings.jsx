/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import {
  CssBaseline,
  ListItem,
  ListItemIcon,
  ListItemText,
  List,
  Drawer
} from '@material-ui/core'

import Icon from '@material-ui/core/Icon'
import { useTabs, APPS } from './config'
import CircularProgress from '@material-ui/core/CircularProgress'
import clsx from 'clsx'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import Grid from '@material-ui/core/Grid'

import './settings.scss'
import { resolvePath, ObjectEqual } from '../../../lib/utils.helper'
import { GetAppsConfFromState } from '../../../lib/markeplace.helper'
import DinamicAppBody from '../../molecules/DinamicAppBody/DinamicAppBody'
import { key as UpdateConfigKey } from '../SettingsList/UpdateManager/config'
import { FindPlatform } from '../../../lib/state.helper'
import * as WifiConfig from '../SettingsList/Wifi/config'
import * as InternetConfig from '../SettingsList/InternetFilter/config'
import * as GamesConfig from '../SettingsList/GamesFilter/config'
import * as WarpConfig from '../SettingsList/Warp/config'

const drawerWidth = 240

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    position: 'relative',
    minHeight: '200px',
    borderTop: '1px solid #F0F0F0',
    backgroundColor: '#F8F8F8'
  },
  hide: {
    display: 'none'
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap'
  },
  drawerOpen: {
    position: 'absolute',
    backgroundColor: '#F0F0F0',
    width: drawerWidth,
    height: '100%',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  drawerClose: {
    position: 'relative',
    backgroundColor: '#F0F0F0',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    overflowX: 'hidden',
    width: theme.spacing(7),
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing(30) + 1
    }
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar
  },
  content: {
    flexGrow: 1,
    display: 'flex',
    padding: theme.spacing(3),
    justifyContent: 'center',
    minHeight: '350px',
    [theme.breakpoints.up('sm')]: {
      paddingLeft: drawerWidth + theme.spacing(3)
    }
  },
  listicon: {
    minWidth: theme.spacing(5)
  }
}))

function LoaderCenter () {
  return (
    <Grid container justifycontent="center" alignItems="center" justify="center">
      <CircularProgress />
    </Grid>
  )
}

const getDeviceApps = (state = {}, userMeta = {}) => {
  const dApps = GetAppsConfFromState(state)
  const hasAwConnect = Object.keys(FindPlatform(state, 'awconnect')).length > 0
  const hasCloudFlare = Object.keys(FindPlatform(state, 'cloudflare')).length > 0
  let apps = { ...APPS }

  if (hasAwConnect) {
    apps = {
      [WifiConfig.key]: { ...WifiConfig },
      ...apps
    }
  }

  if (hasCloudFlare) {
    apps = {
      [WifiConfig.key]: { ...WifiConfig },
      [InternetConfig.key]: { ...InternetConfig },
      [GamesConfig.key]: { ...GamesConfig },
      [WarpConfig.key]: { ...WarpConfig },
      ...apps
    }
  }

  if (Object.keys({ ...apps, ...dApps }).length <= Object.keys(APPS).length) {
    return apps
  }

  const newApps = Object.keys(dApps).reduce((acc, key) => {
    if (
      (
        !dApps[key]['_status'] ||
        dApps[key]['_status'].length === 0
      ) && (
        !dApps[key]['_metaconfig'] ||
        dApps[key]['_metaconfig'].length === 0
      ) && (
        !dApps[key]['_configfiles'] ||
        dApps[key]['_configfiles'].length === 0
      ) && (
        !dApps[key]['_readme'] ||
        dApps[key]['_readme'] === ''
      )
    ) {
      return acc
    }

    const AppBody = (props) => (
      <DinamicAppBody
        appKey={key}
        manifest={dApps[key]}
        {...props}
      />
    )
    const AppIcon = () => <Icon>{dApps[key].icon}</Icon>

    acc[key] = {
      key: key,
      menuText: dApps[key].title,
      Icon: AppIcon,
      Body: AppBody
    }
    return acc
  }, {})

  return {
    ...apps,
    ...newApps
  }
}

const getAppList = (apps) => Object.keys(apps).map((key) => apps[key])

function SettingsInner (props) {
  const classes = useStyles()
  const { tab, selectApp } = useTabs(props.defaultTab)

  const App = props.apps[tab]
  const desktopQuery = useMediaQuery('(min-width:560px)')
  const open = desktopQuery
  return (
    <div className="app-manager">
      <div className={classes.root}>
        <CssBaseline />
        <Drawer
          variant="persistent"
          anchor="left"
          className={clsx(classes.drawer, {
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open
          })}
          classes={{
            paper: clsx({
              [classes.drawerOpen]: open,
              [classes.drawerClose]: !open
            })
          }}
        >
          <List>
            {getAppList(props.apps).map((App, index) => (
              <ListItem
                button
                selected={tab === App.key}
                key={App.key}
                onClick={selectApp(App.key)}>
                <ListItemIcon className={classes.listicon}>
                  <App.Icon />
                </ListItemIcon>
                { desktopQuery && (
                  <ListItemText primary={App.menuText} />
                )}
              </ListItem>
            ))}
          </List>
        </Drawer>
        <section className={classes.content}>
          <React.Suspense
            fallback={<LoaderCenter />}
          >
            {App && App.Body && (<App.Body {...props} />)}
          </React.Suspense>
        </section>
      </div>
    </div>
  )
}

export default class Settings extends React.Component {
  constructor (props) {
    super(props)

    const apps = getDeviceApps(
      resolvePath(props.deviceData, 'activeStep.state'),
      resolvePath(props.deviceData, 'device.user-meta')
    )
    this.state = {
      apps: apps
    }
  }

  shouldComponentUpdate (prevProps, prevState) {
    const deviceStateIsEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'activeStep.state', {}),
      resolvePath(this.props.deviceData, 'activeStep.state', {})
    )

    const userMetasEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'device.user-meta', {}),
      resolvePath(this.props.deviceData, 'device.user-meta', {})
    )

    const deviceMetasEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'device.device-meta', {}),
      resolvePath(this.props.deviceData, 'device.device-meta', {})
    )

    const internalStateIsEqual = ObjectEqual(
      this.state,
      prevState
    )

    return !(userMetasEqual && deviceMetasEqual && deviceStateIsEqual && internalStateIsEqual)
  }

  componentDidUpdate () {
    const apps = getDeviceApps(
      resolvePath(this.props.deviceData, 'activeStep.state'),
      resolvePath(this.props.deviceData, 'device.user-meta')
    )

    this.setState({
      apps: apps
    })
  }

  componentWillUnmount () {
    this.props.togglePolling(true)
  }

  render () {
    const keys = Object.keys(this.state.apps)
    const firstApp = this.state.apps[keys[0]]
    return (
      <SettingsInner
        defaultTab={this.props.deviceData.needUpdate ? UpdateConfigKey : firstApp.key}
        apps={this.state.apps}
        {...this.props}
      />
    )
  }
}
