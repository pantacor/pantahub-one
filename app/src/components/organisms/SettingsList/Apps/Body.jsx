/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { Grid, Box } from '@material-ui/core'
import { connect } from 'react-redux'
import { devicePostState, deviceSetMetadata } from '../../../../store/devices/actions'
import { GetMarketSources, GetAppVersion, USERMETA_MARKETPLACE_KEY } from '../../../../lib/markeplace.helper'
import { GetAppsFromSources } from '../../../../services/marketplace.service'
import { FindPlatform, RemovePlatform } from '../../../../lib/state.helper'
import { resolvePath, ObjectEqual } from '../../../../lib/utils.helper'
import AppsTabs, { APPS_ACTIONS } from './AppsTabs'
import Loading from '../../../atoms/Loading/Loading'

import './apps.scss'

class MarketPlace extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      loading: true,
      apps: [],
      sources: []
    }
  }

  componentDidMount () {
    const sources = GetMarketSources(this.props.deviceData)
    this.setState({
      sources
    })
  }

  componentDidUpdate (prevProps, prevState) {
    try {
      const sources = GetMarketSources(this.props.deviceData)

      if (sources.length !== this.state.sources.length) {
        return this.setState({
          sources
        })
      }

      const stateChanged = !ObjectEqual(
        resolvePath(this.props.deviceData, 'activeStep.state', {}),
        resolvePath(prevProps.deviceData, 'activeStep.state', {})
      )

      if (prevState.sources.length !== this.state.sources.length || stateChanged) {
        return this.getAppsFromSources()
      }
    } catch (e) {
      console.info(e)
    }
  }

  getAppsFromSources = async () => {
    const appsAvailable = await GetAppsFromSources(this.props.token, this.state.sources)
    const apps = await Promise.all(appsAvailable.map(async (app) => {
      const appVersion = await GetAppVersion(app.state, app.key)
      const installedVersion = await GetAppVersion(resolvePath(this.props.deviceData, 'activeStep.state'), app.key)
      const plataform = FindPlatform(resolvePath(this.props.deviceData, 'activeStep.state'), app.key)
      app.installed = Object.keys(plataform).length > 0
      app.hasUpdate = Object.keys(plataform).length > 0 && appVersion !== installedVersion

      return app
    }))

    this.setState({
      apps,
      loading: false
    })
  }

  setNewExternalSource = () => {
    deviceSetMetadata()
  }

  changeAppWithAction = (app, action) => (event) => {
    event.preventDefault()
    let newState = resolvePath(this.props.deviceData, 'activeStep.state')
    let commitAction = ''

    if (action === APPS_ACTIONS.INSTALL_REMOVE && app.installed) {
      newState = RemovePlatform(newState, app.key)
      commitAction = APPS_ACTIONS.REMOVE
    }

    if (action === APPS_ACTIONS.INSTALL_REMOVE && !app.installed) {
      newState = {
        ...newState,
        ...app.state
      }
      commitAction = APPS_ACTIONS.INSTALL
    }

    if (action === APPS_ACTIONS.UPDATE && app.installed) {
      newState = {
        ...newState,
        ...app.state
      }
      commitAction = APPS_ACTIONS.UPDATE
    }

    this.props.devicePostState(
      this.props.deviceData.device.id,
      newState,
      `Application ${commitAction}: ${app.key}`
    )
  }

  saveSources = (sources) => {
    this.props.deviceSetMetadata(
      this.props.deviceData.device.id,
      'user-meta',
      { [USERMETA_MARKETPLACE_KEY]: JSON.stringify(sources) }
    )
  }

  render () {
    if (this.state.loading) {
      return (
        <Box style={{ display: 'flex', justifyItems: 'center', alignItems: 'center' }}>
          <Loading />
        </Box>
      )
    }
    return (
      <Grid
        container
        spacing={4}
        style={{
          width: 'calc(100% + 44px)',
          margin: '-24px'
        }}
        alignContent="stretch"
        alignItems="stretch"
        className="marketplace"
      >
        <AppsTabs
          {...this.props}
          apps={this.state.apps}
          sources={this.state.sources}
          changeAppWithAction={this.changeAppWithAction}
          saveSources={this.saveSources}
        />
      </Grid>
    )
  }
}

export default connect(
  (state) => ({
    token: state.auth.token
  }),
  {
    devicePostState,
    deviceSetMetadata
  }
)(MarketPlace)
