/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import PropTypes from 'prop-types'
import Form from '@rjsf/material-ui'
import SwipeableViews from 'react-swipeable-views'
import { Grid, Tabs, Tab, Box, AppBar } from '@material-ui/core'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import { TrackedButton } from '../../../atoms/Tracker/Tracker'
import { STATUS as DEVICE_STATUS } from '../../../../store/devices/reducer'
import { Track } from '../../../../lib/analytics.helper'

export const APPS_ACTIONS = {
  INSTALL_REMOVE: 'install_remove',
  INSTALL: 'install',
  REMOVE: 'remove',
  UPDATE: 'update'
}

export const SOURCE_ACTIONS = {
  REMOVE: 'remove'
}

function TabPanel (props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
}

function a11yProps (index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`
  }
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%'
  }
}))

function App (props) {
  const {
    app
  } = props
  return (
    <React.Fragment>
      <img src={app.logo} alt={app.title} />
      <h6>{app.title}</h6>
      {app.hasUpdate && (
        <TrackedButton
          style={{ width: '100%', marginBottom: '0.5em' }}
          event="marketplace_update"
          variant="contained"
          color="secondary"
          payload={{ category: 'click', value: `${app.source}#${app.key}` }}
          onClick={props.changeAppWithAction(app, APPS_ACTIONS.UPDATE)}
        >
          Update
        </TrackedButton>
      )}
      <TrackedButton
        style={{ width: '100%' }}
        event={app.installed ? 'marketplace_uninstall' : 'marketplace_install'}
        payload={{ category: 'click', value: `${app.source}#${app.key}` }}
        variant="contained"
        color={app.installed ? 'default' : 'secondary' }
        onClick={props.changeAppWithAction(app, APPS_ACTIONS.INSTALL_REMOVE)}
      >
        {app.installed ? 'Uninstall' : 'Install'}
      </TrackedButton>
    </React.Fragment>
  )
}

function AppList (props) {
  return (
    <Grid container spacing={2}>
      {props.apps.map(app => (
        <Grid
          item
          sm={3}
          xs={6}
          key={`${app.source}-${app.key}`}
          className="app-mini"
        >
          <App
            app={app}
            {...props}
          />
        </Grid>
      ))}
    </Grid>
  )
}

function SourceList (props) {
  const data = {
    sources: props.sources
  }

  const disabled = props.deviceData.status === DEVICE_STATUS.DEPLOY.IN_PROGRESS ||
    props.deviceData.status === DEVICE_STATUS.META.IN_PROGRESS

  const schema = {
    'type': 'object',
    'properties': {
      'sources': {
        'title': '',
        'uniqueItems': true,
        'type': 'array',
        'items': {
          'type': 'string',
          'default': ''
        }
      }
    }
  }

  const uiSchema = {
    'sources': {
      'ui:options': {
        'orderable': false
      }
    }
  }

  const onSubmit = ({ formData }, evnt) => {
    evnt.preventDefault()
    props.saveSources(formData.sources)
  }

  return (
    <Form disabled={disabled} onSubmit={onSubmit} formData={data} schema={schema} uiSchema={uiSchema} />
  )
}

export default function AppsTabs (props) {
  const classes = useStyles()
  const theme = useTheme()
  const [value, setValue] = React.useState(0)

  const handleChange = (event, newValue) => {
    Track('Change Marketplace Tabs', { value: newValue })
    setValue(newValue)
  }

  const handleChangeIndex = (index) => {
    setValue(index)
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs"
        >
          <Tab label="Applications" {...a11yProps(0)} />
          <Tab label="Sources" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        <TabPanel value={value} index={0} dir={theme.direction}>
          <AppList
            {...props}
          />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <SourceList
            {...props}
          />
        </TabPanel>
      </SwipeableViews>
    </div>
  )
}

AppsTabs.propTypes = {
  saveSources: PropTypes.func.isRequired,
  changeAppWithAction: PropTypes.func.isRequired,
  sources: PropTypes.array.isRequired,
  apps: PropTypes.array.isRequired
}
