/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState, useEffect } from 'react'
import { connect } from 'react-redux'
import { Grid, useMediaQuery, Button, Typography, LinearProgress } from '@material-ui/core'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import { resolvePath } from '../../../../lib/utils.helper'
import { deviceSetMetadata, devicePostStep, GetDeviceTrail } from '../../../../store/devices/actions'
import { CURRENT_STEP_NOTIFICATIONS } from '../../../../lib/const'
import { FindPlatform } from '../../../../lib/state.helper'
import { BlockSwitchInv } from '../../../atoms/BlockSwitch/BlockSwitch'
import { GetDeviceModel, GetSourceIdByModel } from '../../../../lib/devices.helper'
import { GetDeviceByShortname } from '../../../../services/devices.service'
import warpLogo from '../../../../assets/images/warp_logo.png'
import { Track } from '../../../../lib/analytics.helper'

const KEY = 'ph.vpn.warp'
const wgKey = 'wg0'

const getWgInterfaces = (deviceData) => {
  const interfaces = resolvePath(deviceData, 'device.device-meta.interfaces', {})
  return Object.keys(interfaces).reduce((acc, key) => {
    if (key.indexOf(wgKey) >= 0) {
      acc.push({ name: key, value: interfaces[key] })
    }
    return acc
  }, [])
}

let phVpnApp = {}

const useMountEffect = (fun) => useEffect(fun, [])
const getWarpConfig = (deviceData) => resolvePath(deviceData, 'device.user-meta', {})['ph.vpn.warp']

const hasPhVpnPlatformInstaller = (deviceData) => {
  return FindPlatform(resolvePath(deviceData, 'activeStep.state', {}), 'ph-vpn')
}

const getMergeStateWithInstall = (deviceData) => {
  return {
    ...resolvePath(deviceData, 'activeStep.state', {}),
    ...phVpnApp
  }
}

const buildCommit = (deviceData) => {
  return {
    'commit-msg': 'Install ph-vpn with Warp',
    rev: -1,
    state: getMergeStateWithInstall(deviceData)
  }
}

const isChecked = (deviceData) => {
  return getWarpConfig(deviceData) === 'enabled'
}

function Interfaces ({ interfaces = [], activated }) {
  if (interfaces.length <= 0 && !activated) {
    return null
  }

  if (interfaces.length > 0 && !activated) {
    return (
      <Grid container justify='center'spacing={2} >
        <Grid item xs={12}>
          Disconnecting from Warp
        </Grid>
        <Grid item xs={12}>
          <LinearProgress />
        </Grid>
      </Grid>
    )
  }

  if (interfaces.length <= 0 && activated) {
    return (
      <Grid container justify='center'spacing={2} >
        <Grid item xs={12}>
          Connecting to Warp
        </Grid>
        <Grid item xs={12}>
          <LinearProgress />
        </Grid>
      </Grid>
    )
  }

  return (
    <Grid container justify='center' spacing={2} >
      <Typography variant='body1'>
        Connected Interfaces
      </Typography>
      {interfaces.map((interf, key) => (
        <Grid key={key} item container xs={12}>
          <Grid item xs={6}>
            {interf.name}
          </Grid>
          <Grid item xs={6}>
            {(interf.value || []).join(',  ')}
          </Grid>
        </Grid>
      ))}
    </Grid>
  )
}

function WarpBody (props) {
  const [checked, setChecked] = useState(isChecked(props.deviceData))
  const desktopQuery = useMediaQuery('(min-width:560px)')

  async function getLatestVpn () {
    try {
      const model = GetDeviceModel(resolvePath(props, 'deviceData.device', {}))
      const deviceResp = await GetDeviceByShortname(this.props.token, GetSourceIdByModel(model))
      if (!deviceResp.ok) {
        return
      }
      const resp = await GetDeviceTrail(props.token, deviceResp.json.id, '')
      if (resp.ok) {
        phVpnApp = FindPlatform(resp.json.activeStep.state)
      }
    } catch (_) {}
  }

  useMountEffect(() => {
    getLatestVpn()
  })

  const handleChange = (event) => {
    event.preventDefault()

    Track('Wrap', { value: event.target.checked ? 'enabled' : 'disabled' })

    props.deviceSetMetadata(props.deviceData.device.id, 'user-meta', { [KEY]: event.target.checked ? 'enabled' : 'disabled' })
  }

  const hanldeInstall = (event) => {
    event.preventDefault()
    props.devicePostStep(props.deviceData.device.id, buildCommit(props.deviceData))
  }

  useEffect(() => {
    setChecked(isChecked(props.deviceData))
  }, [props, props.deviceData])

  if (!hasPhVpnPlatformInstaller(props.deviceData)) {
    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid item xs={12} sm={6}>
          <Button
            variant="outlined"
            size="large"
            color="primary"
            disabled={props.activeStepNotification === CURRENT_STEP_NOTIFICATIONS.LOADING || props.deviceData.loadingDevice}
            onClick={hanldeInstall}
          >
            Install Warp
          </Button>
        </Grid>
      </Grid>
    )
  }

  return (
    <Grid
      container
      direction="row"
      justify="center"
      alignItems="center"
    >
      <Grid item xs={6} sm={6}>
        <img
          alt="warp"
          style={{ width: '200px', maxWidth: '100%' }}
          src={warpLogo}
        />
      </Grid>
      <Grid container item xs={6} sm={6} justify="center">
        <FormGroup style={{ justifyItems: 'flex-start' }}>
          {!desktopQuery ? (
            <Grid container justify="center">
              <BlockSwitchInv
                size="medium"
                checked={checked}
                onChange={handleChange}
                name="warpActive"
                inputProps={{ 'aria-label': 'Activated' }}
              />
            </Grid>
          ) : (
            <FormControlLabel
              style={{ display: 'block' }}
              control={
                <BlockSwitchInv
                  size="medium"
                  checked={checked}
                  onChange={handleChange}
                  name="warpActive"
                  inputProps={{ 'aria-label': 'Activated' }}
                />
              }
              label={checked ? 'Activated' : 'Deactivated'}
            />
          )}
        </FormGroup>
      </Grid>
      <Grid item xs={12} sm={6}>
        <Interfaces activated={checked} interfaces={getWgInterfaces(props.deviceData)} />
      </Grid>
    </Grid>
  )
}

export default connect(
  (state) => ({
    token: state.auth.token
  }),
  { deviceSetMetadata, devicePostStep }
)(WarpBody)
