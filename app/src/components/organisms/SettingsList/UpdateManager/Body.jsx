/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { connect } from 'react-redux'
import UpdateIcon from '@material-ui/icons/Update'
import { Grid, Typography, Box } from '@material-ui/core'
import { devicePostState } from '../../../../store/devices/actions'
import Loading from '../../../atoms/Loading/Loading'
import { GetPlatforms, GetPosibleUpdates, GetPlatformsWithUpdate } from '../../../../lib/state.helper'
import { resolvePath, ObjectEqual } from '../../../../lib/utils.helper'
import { TrackedButton } from '../../../atoms/Tracker/Tracker'
import { Track } from '../../../../lib/analytics.helper'

export const STATUS = {
  NONE: 'none',
  LOADING: 'loading',
  SUCCESS: 'success',
  UPDATING: 'updating',
  ERROR: 'error'
}

const getUpdateState = (device, source, updates) => {
  const newState = { ...device }
  updates.forEach((update) => {
    Object.keys(source).forEach((key) => {
      if (key.indexOf(update) >= 0 && key.indexOf('_config') < 0) {
        newState[key] = source[key]
      }
    })
  })
  return newState
}

class UpdateManager extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      status: STATUS.LOADING,
      error: null,
      source: {},
      devicePlatforms: {},
      sourcePlatforms: {},
      platformsWithUpdate: []
    }
  }

  componentDidMount () {
    return this.getPossibleUpdate()
  }

  shouldComponentUpdate (prevProps, prevState) {
    const deviceStateIsEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'activeStep.state', {}),
      resolvePath(this.props.deviceData, 'activeStep.state', {})
    )

    const userMetasEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'device.user-meta', {}),
      resolvePath(this.props.deviceData, 'device.user-meta', {})
    )

    const internalStateIsEqual = ObjectEqual(
      this.state,
      prevState
    )

    return !(deviceStateIsEqual && userMetasEqual && internalStateIsEqual)
  }

  componentDidUpdate () {
    this.updatePlatformWithUpdate()
  }

  updatePlatformWithUpdate = (deviceState, sourceState) => {
    const devicePlatforms = GetPlatforms(resolvePath(this.props, 'deviceData.activeStep.state', {}))

    this.setState({
      platformsWithUpdate: GetPlatformsWithUpdate(devicePlatforms, this.state.sourcePlatforms)
    })
  }

  getPossibleUpdate = () => {
    const comparation = GetPosibleUpdates(
      this.props.baseSystem,
      resolvePath(this.props, 'deviceData.activeStep.state', {}),
      resolvePath(this.props, 'deviceData.device', {})
    )

    this.setState({
      ...this.state,
      status: STATUS.SUCCESS,
      source: comparation.sourceState,
      sourcePlatforms: comparation.sourcePlatforms,
      devicePlatforms: comparation.devicePlatforms,
      platformsWithUpdate: comparation.platformsWithUpdate
    })
  }

  onUpdateSystemClick = async (event) => {
    event.preventDefault()
    const newState = getUpdateState(
      resolvePath(this.props, 'deviceData.activeStep.state', {}),
      this.state.source,
      this.state.platformsWithUpdate
    )

    Track('Update System')

    this.props.devicePostState(
      this.props.deviceData.device.id,
      newState,
      `System platforms update: ${this.state.platformsWithUpdate.join(', ')}`
    )
  }

  render () {
    if (this.state.status === STATUS.LOADING) {
      return (
        <Box style={{ display: 'flex', justifyItems: 'center', alignItems: 'center' }}>
          <Loading />
        </Box>
      )
    }

    if (this.state.platformsWithUpdate.length === 0) {
      return (
        <Grid container spacing={2} justify='center' alignItems='center' alignContent="center">
          <Grid item xs={12}>
            <UpdateIcon fontSize="large" />
          </Grid>
          <Typography variant='body1'>
            Your system is up to date
          </Typography>
        </Grid>
      )
    }

    return (
      <Grid container spacing={2} justify='center' alignItems='center' alignContent="center">
        <Grid item xs={12}>
          <UpdateIcon fontSize="large" />
        </Grid>
        <Grid item xs={12}>
          <Typography variant="h6">
            A System Update is available!
          </Typography>
          <TrackedButton
            event="update-system"
            variant="outlined"
            color="primary"
            style={{ marginTop: '1em' }}
            onClick={this.onUpdateSystemClick}
          >
            Update now
          </TrackedButton>
        </Grid>
      </Grid>
    )
  }
}

export default connect(
  (state) => ({
    token: state.auth.token,
    baseSystem: state.baseSystem
  }),
  {
    devicePostState
  }
)(UpdateManager)
