/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { resolvePath } from '../../../lib/utils.helper'
import { Grid } from '@material-ui/core'
import Timeago from '../../atoms/Timeago/Timeago'
import DeviceLoginButton from '../../atoms/DeviceLoginButton/DeviceLoginButton'
import IfConnectedToRouter from '../../atoms/IfConnectedToRouter/IfConnectedToRouter'

const getIp = (device) => {
  const interfaces = resolvePath(device, 'device-meta.interfaces', {})
  return (interfaces['wlan0.ipv4'] || [])[0]
}

export default function LocalDevice ({ device }) {
  return (
    <Grid
      container
      className="local-device mt--1 mb--1"
      alignItems="center"
      alignContent="center"
    >
      <Grid
        item
        xs={12}
        md={3}
        className="device-name"
      >
        {device.nick}
      </Grid>
      <Grid
        item
        xs={12}
        md={3}
        className="device-time-created"
      >
        <Timeago when={device['time-created']} />
      </Grid>
      <Grid
        item
        xs={12}
        md={3}
        className="device-ip"
      >
        {getIp(device)}
      </Grid>
      <Grid
        item
        xs={12}
        md={3}
        className="device-ip"
      >
        <IfConnectedToRouter
          ip={getIp(device)}
          id={device.id}
          NoConnected={() => (
            <React.Fragment>
              Please connect to Wifi-hotspot
            </React.Fragment>
          )}
        >
          <DeviceLoginButton
            deviceip={getIp(device)}
          />
        </IfConnectedToRouter>
      </Grid>
    </Grid>
  )
}
