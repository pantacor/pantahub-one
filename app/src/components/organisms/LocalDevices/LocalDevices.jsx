/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { useState } from 'react'
import Grid from '@material-ui/core/Grid'
import { DeviceCount, GetDevices } from '../../../lib/localdevices.helper'
import { ObjectToArray } from '../../../lib/utils.helper'
import { Container, Typography } from '@material-ui/core'
import LocalDevicesHeader from './LocalDevicesHeader'
import LocalDevice from './LocalDevice'
import PhLoginButton from '../../atoms/PhLoginButton/PhLoginButton'
import DeviceLoginButton from '../../atoms/DeviceLoginButton/DeviceLoginButton'

// const getIp = (device) => {
//   const interfaces = resolvePath(device, 'device-meta.interfaces', {})
//   return (interfaces['wlan0.ipv4'] || [])[0]
// }

export function ConnectionButtons ({ noLocal = false }) {
  return (
    <Grid
      container
      spacing={2}
      direction="row"
      justify="center"
      alignContent="center"
      alignItems="center"
      className="connection-buttons"
    >
      <Grid item xs={12} md={8}>
        <Typography variant="body1" className="mt--1">
          You can manage your devices from anywhere by connecting your Pantahub account,
          or connect to your One hotspot and come back to this page to manage it locally
        </Typography>
      </Grid>
      <Grid item xs={12} md={6}>
        <Grid
          container
          spacing={2}
          justify="center"
          alignContent="center"
          alignItems="center"
          className="mt--1"
        >
          {!noLocal && (
            <Grid item xs={12} md={6}>
              <DeviceLoginButton
                variant="outlined"
                color="primary"
                deviceip="10.50.0.1"
                text="Manage my router"
              />
            </Grid>
          )}
          <Grid item xs={12} md={6}>
            <PhLoginButton
              variant="outlined"
              color="primary"
              noImage={true}
              text="Manage with PantacorHub"
            >
            </PhLoginButton>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  )
}

export default function LocalDevices () {
  const [availableDevices] = useState([])
  const count = DeviceCount()
  const devices = ObjectToArray(GetDevices())

  // useEffect(() => {
  //   async function GetDevicesAvailable () {
  //     const availables = await devices.reduce(async (acc, device) => {
  //       try {
  //         const connected = await TestImageUrl(GetLocalDeviceImageUrl(getIp(device), device.id))
  //         if (connected) {
  //           acc.push(device.id)
  //         }
  //       } catch (e) {}
  //       return acc
  //     }, [])

  //     if (availables.length !== availableDevices.length) {
  //       setAvailableDevices(availables)
  //     }
  //   }
  //   if (count > 0) {
  //     GetDevicesAvailable()
  //   }
  // }, [devices, availableDevices, availableDevices.length, count])

  return (
    <div className="hero-bg-color">
      <Container>
        <Grid
          container
          direction="row"
          spacing={2}
          alignItems="center"
          justify="center"
          className="mt--1 local-devices"
        >
          {availableDevices.length <= 0 || count <= 0 ? (
            <ConnectionButtons />
          ) : (
            <React.Fragment>
              <Grid item xs={12} md={8}>
                <Typography className="text-align-left" variant="body1">
                  {`Your session is lost, but don't worry you can manage your local devices. Please connect to your WIFI hotspot and select the device you want to manage`}
                </Typography>
              </Grid>
              <Grid item xs={12} md={8}>
                <LocalDevicesHeader />
                {devices.map((device) => (
                  <LocalDevice key={device.id} device={device} />
                ))}
              </Grid>
            </React.Fragment>
          )}
        </Grid>
      </Container>
    </div>
  )
}
