/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import React, { Component } from 'react'
import { connect } from 'react-redux'
import Grid from '@material-ui/core/Grid'
import Fade from '@material-ui/core/Fade'
import { Done, Warning } from '@material-ui/icons'
import {
  getDevice,
  getTrail,
  deviceSetMetadata,
  devicePatch,
  devicePostState,
  installPlatform,
  uninstallPlatform
} from '../../../store/devices/actions'

import DeviceHeader from '../../molecules/DeviceHeader/DeviceHeader'
import { CURRENT_STEP_NOTIFICATIONS } from '../../../lib/const'
import { resolvePath, ObjectEqual } from '../../../lib/utils.helper'
import { EXPANDED_TABS, togleStatus } from '../../../lib/devices.helper'
import Settings from '../Settings/Settings'
import { STATUS as DEVICE_STATUS } from '../../../store/devices/reducer'
import LoadingModal from '../../molecules/LoadingModal/LoadingModal'

const BodyElements = {
  [EXPANDED_TABS.NONE]: null,
  [EXPANDED_TABS.SETTINGS_MANAGER]: Settings
}

class Device extends Component {
  constructor (props) {
    super(props)
    this.interval = null

    this.elementRef = React.createRef()
    this.state = {
      expanded: EXPANDED_TABS.NONE,
      loading: true,
      showSuccess: false,
      showWarning: false
    }
  }

  shouldComponentUpdate = (prevProps, prevState) => {
    const internalStateEqual = ObjectEqual(
      this.state,
      prevState
    )
    const userMetasEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'device.user-meta', {}),
      resolvePath(this.props.deviceData, 'device.user-meta', {})
    )

    const deviceMetasEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'device.device-meta', {}),
      resolvePath(this.props.deviceData, 'device.device-meta', {})
    )

    const stateEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'currentStep', {}),
      resolvePath(this.props.deviceData, 'currentStep', {})
    )

    const needUpdateEqual =
      resolvePath(prevProps.deviceData, 'needUpdate', false) ===
      resolvePath(this.props.deviceData, 'needUpdate', false)

    const deviceNotPosting = this.props.deviceData.status !== DEVICE_STATUS.DEPLOY.IN_PROGRESS
    const metaNotPosting = this.props.deviceData.status !== DEVICE_STATUS.META.IN_PROGRESS

    return !(
      needUpdateEqual &&
      deviceNotPosting &&
      metaNotPosting &&
      internalStateEqual &&
      deviceMetasEqual &&
      userMetasEqual &&
      stateEqual
    )
  }

  componentDidMount = () => {
    const { deviceData } = this.props
    const { device, loadingTrail, currentStep } = deviceData

    if (!currentStep && !loadingTrail) {
      this.getFirstTrail(device.id)
    }

    this.setInterval()
  }

  onExpandClick = (type) => (evnt) => {
    const { deviceData } = this.props
    const { platformInstalled } = deviceData

    if (this.elementRef.current &&
      platformInstalled &&
      !this.elementRef.current.querySelector('.editable-area').contains(evnt.target)
    ) {
      const newStatus = togleStatus(type, this.state.expanded)
      // this.props.togglePolling(newStatus === EXPANDED_TABS.NONE)
      this.setState({ expanded: newStatus })
    }
  }

  componentDidUpdate = (prevProps, prevState, snapshot) => {
    if (prevProps.deviceData === this.props.deviceData) {
      return
    }

    if (prevState.loading && this.state.loading && !this.props.deviceData.loadingTrail) {
      this.setState({ loading: false })
      return
    }

    const { deviceData = {} } = this.props
    const prevDeviceData = prevProps.deviceData || {}

    const previousDone = resolvePath(prevDeviceData, 'currentStep.progress.status') === 'UPDATED' ||
      resolvePath(deviceData, 'currentStep.progress.status') === 'DONE'

    // status changed from not DONE to DONE
    // if current step is in done status. if previous status was marked as not done, check the current step revision
    // the step that was loading now is done, show the success notification
    if (
      !previousDone &&
      resolvePath(deviceData, 'currentStep.rev') &&
      resolvePath(prevDeviceData, 'currentStep.rev') &&
      resolvePath(deviceData, 'currentStep.rev') === resolvePath(prevDeviceData, 'currentStep.rev')
    ) {
      this.showSuccessNotification()
      return
    }

    // the current step is different from the one that was loading. show the warning notification
    if (
      !previousDone &&
      resolvePath(deviceData, 'currentStep.rev') &&
      resolvePath(deviceData, 'activeStep.rev') &&
      resolvePath(deviceData, 'currentStep.rev') !== resolvePath(deviceData, 'activeStep.rev')
    ) {
      this.showWarningNotification()
    }
  }

  showSuccessNotification = () => {
    this.setState({ showSuccess: true })
    setTimeout(() => {
      this.hideNotification()
    }, process.env.REACT_APP_NOTIFICATION_DURATION || 5000)
  }

  showWarningNotification = () => {
    this.setState({ showWarning: true })
    setTimeout(this.hideNotification, process.env.REACT_APP_NOTIFICATION_DURATION || 5000)
  }

  hideNotification = () => {
    this.setState({ showSuccess: false, showWarning: false })
  }

  getFirstTrail = () => {
    const deviceId = resolvePath(this.props, 'deviceData.device.id')
    if (!deviceId) { return }
    if (this.interval !== null) { return }

    this.props.getTrail(deviceId)
  }

  setInterval = () => {
    if (this.interval === null) {
      this.interval = setInterval(this.updateTrail, process.env.REACT_APP_REFRESH_TIME || 10000)
    }
  }

  clearInterval = () => {
    if (this.interval !== null) {
      clearInterval(this.interval)
    }
    this.interval = null
  }

  componentWillUnmount = () => {
    this.clearInterval()
  }

  updateTrail = () => {
    const { deviceData, getTrail } = this.props
    const device = (deviceData ? deviceData.device : null)

    if (getTrail && device) {
      getTrail(device.id)
    }
  }

  currentStepNotification = () => {
    const DONE = resolvePath(this.props, 'deviceData.currentStep.progress.status') === 'UPDATED' ||
      resolvePath(this.props, 'deviceData.currentStep.progress.status') === 'DONE'

    if (
      resolvePath(this.props, 'deviceData.currentStep.progress.status') === 'ERROR' ||
      resolvePath(this.props, 'deviceData.currentStep.progress.status') === 'WONTGO'
    ) {
      return CURRENT_STEP_NOTIFICATIONS.ERROR
    }

    if (
      this.props.deviceData.status === DEVICE_STATUS.DEPLOY.IN_PROGRESS ||
      this.state.loading ||
      !DONE
    ) {
      return CURRENT_STEP_NOTIFICATIONS.LOADING
    }

    if (this.state.showSuccess) {
      return CURRENT_STEP_NOTIFICATIONS.SUCCESS
    }

    if (this.state.showWarning) {
      return CURRENT_STEP_NOTIFICATIONS.WARNING
    }

    return CURRENT_STEP_NOTIFICATIONS.NONE
  }

  render () {
    const { deviceData, classes } = this.props
    const { currentStep } = deviceData

    if (!currentStep) {
      return (
        <Fade in={true} ref={this.elementRef}>
          <Grid container spacing={2} justify='flex-end'>
            <Grid item xs={12} className={classes.deviceContainer}>
              <DeviceHeader
                {...this.props}
                {...this.state}
                currentStepNotification={this.currentStepNotification()}
                onExpandClick={this.onExpandClick}
              />
            </Grid>
          </Grid>
        </Fade>
      )
    }

    const Body = BodyElements[this.state.expanded]
    return (
      <Fade in={!!currentStep} ref={this.elementRef}>
        <Grid container spacing={2} justify='center'>
          <Grid item xs={12} className={classes.deviceContainer}>
            <DeviceHeader
              {...this.props}
              {...this.state}
              currentStepNotification={this.currentStepNotification()}
              onExpandClick={this.onExpandClick}
            />
            {Body && (
              <section style={{ position: 'relative' }}>
                {this.currentStepNotification() === CURRENT_STEP_NOTIFICATIONS.LOADING && (
                  <LoadingModal {...this.props} />
                )}
                {this.currentStepNotification() === CURRENT_STEP_NOTIFICATIONS.SUCCESS &&
                  (<Done className={`${classes.deviceBodyModal} ${classes.deviceSuccessNotification}`} />)}
                {this.currentStepNotification() === CURRENT_STEP_NOTIFICATIONS.WARNING &&
                  (<Warning className={`${classes.deviceBodyModal} ${classes.deviceWarningNotification}`} />)}
                <Body
                  {...this.props}
                  {...this.state}
                  currentStepNotification={this.currentStepNotification()}
                  onExpandClick={this.onExpandClick}
                />
              </section>
            )}
          </Grid>
        </Grid>
      </Fade>
    )
  }
}

export default connect(
  (state) => ({
    user: state.auth
  }),
  {
    getDevice,
    getTrail,
    deviceSetMetadata,
    devicePatch,
    devicePostState,
    installPlatform,
    uninstallPlatform
  }
)(Device)
