/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* eslint-disable no-restricted-globals */
import React, { useEffect } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import { connect } from 'react-redux'
import { Login, GetUser, SaveTokenAndRedirect } from '../../../store/auth/actions'
import { STATUS as UserStatus } from '../../../store/auth/reducer'
import MainLayout from '../../layouts/Main'
import Home from '../../pages/Home'

function AuthenticationContainer (props) {
  const {
    status,
    token,
    Login,
    GetUser,
    SaveTokenAndRedirect,
    error
  } = props

  useEffect(() => {
    const hash = window.location.hash

    if (hash !== '') {
      SaveTokenAndRedirect(hash)
    }
  }, [token, SaveTokenAndRedirect, Login])

  useEffect(() => {
    if (status === null) {
      GetUser()
    }
  }, [GetUser, status])

  if (status === UserStatus.INPROGRESS) {
    return (
      <CircularProgress />
    )
  }
  // const hash = window.location.hash
  const isLogged = !!token && token !== ''

  return (
    <div className="App" style={{ overflow: 'hidden' }}>
      <MainLayout status={status} isLogged={isLogged} error={error}>
        <Home status={status} isLogged={isLogged} />
      </MainLayout>
    </div>
  )
}

export default connect(
  (state) => ({
    token: state.auth.token,
    status: state.auth.status,
    error: state.errors
  }),
  {
    Login,
    GetUser,
    SaveTokenAndRedirect
  }
)(AuthenticationContainer)
