/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState } from 'react'
import Grid from '@material-ui/core/Grid'
import { Wifi, VpnKey, Security, Router } from '@material-ui/icons'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import {
  RADIO_24GHZ,
  RADIO_5GHZ,
  SECURITY_WPA1,
  SECURITY_WPA2 } from '../../../store/devices/platformSetup'

export function Form (props) {
  const {
    classes,
    ssid,
    psk,
    security,
    radio,
    biggerThanMediumScreen,
    onChangeText
  } = props
  const [ passwordVisible, setPasswordVisible ] = useState(false)

  const onPasswordVisibilityClick = () => {
    setPasswordVisible(!passwordVisible)
  }

  return (
    <form autoComplete="off">
      <Grid
        container
        item
        xs={12}
        className={(biggerThanMediumScreen ? classes.deviceBodyFormLarge : classes.deviceBodyFormSmall)}
      >
        <Grid
          container
          item
          md={6}
          xs={12}
          justify='center'
          alignItems='flex-end'
          spacing={1}
          className={classes.deviceBodyFormBlock}
        >
          <Grid item xs={2} >
            <Wifi />
          </Grid>
          <Grid item xs={10} >
            <TextField
              autoComplete="off"
              label='Network Name'
              className={`${classes.fullWidth}`}
              value={ssid}
              onChange={evt => onChangeText('ssid', evt.target.value)}
            />
          </Grid>
        </Grid>
        <Grid
          container
          item
          md={6}
          xs={12}
          justify='center'
          alignItems='flex-end'
          spacing={1}
          className={classes.deviceBodyFormBlock}
        >
          <Grid item xs={2} >
            <VpnKey />
          </Grid>
          <Grid item xs={10} className={classes.deviceBodyFormBlockInputItem}>
            <TextField
              autoComplete="off"
              label='Network Password'
              className={`${classes.fullWidth}`}
              type={(passwordVisible ? 'text' : 'password')}
              value={psk} onChange={evt => onChangeText('psk', evt.target.value)}
            />
            <Button
              className={`${classes.deviceEditableButtons}
              ${classes.deviceNetworkPasswordVisibleButton}`}
              size='small'
              onClick={onPasswordVisibilityClick}
            >
              {(passwordVisible ? 'hide' : 'show')}
            </Button>
          </Grid>
        </Grid>
        <Grid
          container
          item
          md={6}
          xs={12}
          justify='center'
          alignItems='flex-end'
          spacing={1}
          className={classes.deviceBodyFormBlock}
        >
          <Grid item xs={2} >
            <Router />
          </Grid>
          <Grid item xs={10} className="select-align-left" >
            <Select
              id="wifi-radio"
              className={classes.fullWidth}
              style={{ textAlign: 'center' }}
              value={radio}
              onChange={evt => onChangeText('radio', evt.target.value)}
            >
              <MenuItem value={RADIO_24GHZ}>2.4Ghz</MenuItem>
              <MenuItem value={RADIO_5GHZ}>5Ghz</MenuItem>
            </Select>
          </Grid>
        </Grid>
        <Grid
          container
          item
          md={6}
          xs={12}
          justify='center'
          alignItems='flex-end'
          spacing={1}
          className={classes.deviceBodyFormBlock}
        >
          <Grid item xs={2} >
            <Security />
          </Grid>
          <Grid item xs={10} className="select-align-left" >
            <Select
              id="wifi-security"
              className={classes.fullWidth}
              style={{ textAlign: 'center' }}
              value={security}
              onChange={evt => onChangeText('security', evt.target.value)}
            >
              <MenuItem value={SECURITY_WPA1}>WPA1</MenuItem>
              <MenuItem value={SECURITY_WPA2}>WPA2</MenuItem>
            </Select>
          </Grid>
        </Grid>
      </Grid>
    </form>
  )
}
