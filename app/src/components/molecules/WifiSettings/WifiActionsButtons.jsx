/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import Grid from '@material-ui/core/Grid'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

export default function WifiActionsButtons (props) {
  const { classes, hasChanges, onAcceptClick, onExpandClick } = props
  const biggerThanMediumScreen = useMediaQuery('(min-width:960px)') // Material UI medium size
  const mobile = useMediaQuery('(max-width:560px)') // Material UI medium size

  const buttonSize = (biggerThanMediumScreen ? 'medium' : 'small')

  return (
    <Grid
      container
      item
      xs={12}
      className={classes.deviceBodyButtonsBlock}
      justify="flex-end"
      spacing={2}
    >
      <Grid item sm="auto" xs={12}>
        <TrackedButton
          event="apply-wifi-changes"
          variant='contained'
          color='primary'
          style={{ marginBottom: mobile ? '1em' : '0' }}
          className={(hasChanges ? '' : classes.colorDisabled)}
          onClick={onAcceptClick}
          size={buttonSize}
          disabled={!hasChanges}
        >
          Apply
        </TrackedButton>
      </Grid>
      <Grid item sm="auto" xs={12}>
        <TrackedButton
          event="cancel-wifi-changes"
          variant='contained'
          color='default'
          style={{ marginBottom: mobile ? '1em' : '0' }}
          onClick={onExpandClick}
          size={buttonSize}>
          Cancel
        </TrackedButton>
      </Grid>
    </Grid>
  )
}
