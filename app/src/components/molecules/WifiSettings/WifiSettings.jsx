/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { PureComponent } from 'react'
import { getWifiHotspotJsonEntry, getWifiHotspotJsonKey, mapWifiStateToWifiHotspot } from '../../../store/devices/platformSetup'
import Grid from '@material-ui/core/Grid'
import WifiActionsButtons from './WifiActionsButtons'
import { Form } from './Form'
import { ObjectEqual } from '../../../lib/utils.helper'
import { Track } from '../../../lib/analytics.helper'

export default class WifiSettings extends PureComponent {
  constructor (props) {
    super(props)

    const { ssid, psk, radio, security } = this.getDataFromProps()
    this.state = { ssid, psk, radio, security, hasChanges: false, reloadState: false }
  }

  getDataFromProps = () => {
    const { deviceData } = this.props
    const { activeStep } = deviceData
    const state = (activeStep ? activeStep.state : null)
    // eslint-disable-next-line no-unused-vars
    const [_, wifiHotspotValue] = getWifiHotspotJsonEntry(state)
    return { ...wifiHotspotValue }
  }

  onChangeText = (field, value) => {
    this.setState({ [field]: value, hasChanges: true })
  }

  onAcceptClick = () => {
    Track('Change Wifi Settings')

    const { deviceData, devicePostState } = this.props
    const { device, activeStep } = deviceData
    const state = (activeStep ? activeStep.state : null)
    const wifiHotspotKey = getWifiHotspotJsonKey(state)
    if (state && wifiHotspotKey) {
      // Create new state
      const newState = {
        ...state,
        [wifiHotspotKey]: mapWifiStateToWifiHotspot({
          ssid: this.state.ssid,
          psk: this.state.psk,
          security: this.state.security,
          radio: this.state.radio
        })
      }
      devicePostState(device.id, newState, 'WifiHotspot ssid and psk updated from One app')
      this.setState({ hasChanges: true, reloadState: true })
      Track('Change Wifi Settings Success')
    } else {
      if (!state) {
        throw new Error('Device state not found!')
      }
      if (!wifiHotspotKey) {
        throw new Error('Device wifiHotspotKey not found!')
      }
      Track('Change Wifi Settings Error')
    }
  }

  shouldComponentUpdate (prevProps, prevState) {
    const stateEqual = ObjectEqual(
      prevState,
      this.state
    )

    const propsEqual = ObjectEqual(
      prevProps,
      this.props
    )

    return !(stateEqual && propsEqual)
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    if (prevProps.deviceData !== this.props.deviceData) {
      const { ssid, psk, radio, security } = this.getDataFromProps()
      this.setState({
        ssid,
        psk,
        radio,
        security,
        hasChanges: false,
        reloadState: false
      })
    }
  }

  render () {
    const {
      deviceData,
      classes,
      biggerThanMediumScreen,
      onExpandClick,
      uninstallPlatform
    } = this.props

    return (
      <Grid
        container
        justify='center'
        alignItems="center"
        className={classes.deviceBody}
      >
        <Form
          deviceData={deviceData}
          classes={classes}
          biggerThanMediumScreen={biggerThanMediumScreen}
          ssid={this.state.ssid}
          psk={this.state.psk}
          radio={this.state.radio}
          security={this.state.security}
          onChangeText={this.onChangeText}
        />
        <WifiActionsButtons
          deviceData={deviceData}
          classes={classes}
          biggerThanMediumScreen={biggerThanMediumScreen}
          hasChanges={this.state.hasChanges}
          onAcceptClick={this.onAcceptClick}
          onExpandClick={onExpandClick}
          uninstallPlatform={uninstallPlatform}
        />
      </Grid>
    )
  }
}
