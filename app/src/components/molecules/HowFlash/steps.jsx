/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import PhLoginButton from '../../atoms/PhLoginButton/PhLoginButton'
import DeviceLoginButton from '../../atoms/DeviceLoginButton/DeviceLoginButton'
import Grid from '@material-ui/core/Grid'
import { Tracker } from '../../atoms/Tracker/Tracker'
import ImageButton from '../ImageButton/ImageButton'
import DownloadImager from '../DownloadImager/DownloadImager'
import InstallGIF from '../../../assets/images/install.gif'
import Warning from '../../../assets/images/warning.png'
import AnonimousLoginButton from '../../atoms/AnonimousLoginButton/AnonimousLoginButton'
import { grey } from '@material-ui/core/colors'
import { Wifi, VpnKey } from '@material-ui/icons'

export const SIGNIN = {
  key: 'connect-account',
  title: 'Connect your Pantahub account',
  content: (
    <React.Fragment>
      <Grid
        container
        spacing={2}
        direction="row"
        justify="center"
        alignItems="center"
      >
        <Grid
          item
          md={8}
          xs={12}
        >
          {`Use Google, Gitlab or Github if you don't have a Pantahub account`}
          <br/>
          <br/>
          <p>
            <span style={{ display: 'inline-flex', alignItems: 'center' }} >
              <img src={Warning} height='20px' alt="warning"/>{' '}
              <b>  IMPORTANT NOTICE: </b>
            </span>
            <br/>
            To test without signing up, skip this and go to Step 2<br/>You will use an anonymous session that expires after two days
          </p>
        </Grid>
      </Grid>
      <Grid
        container
        spacing={2}
        direction="row"
        justify="center"
        alignItems="center"
        className="content"
      >
        <PhLoginButton />
        <AnonimousLoginButton />
        <DeviceLoginButton />
      </Grid>
    </React.Fragment>
  )
}

export const DOWNLOAD_IMAGE = {
  key: 'download-image',
  title: 'Download Pantavisor for your Raspberry Pi',
  content: (
    <Grid
      className="mt--2"
      container
      spacing={2}
      direction="row"
      justify="flex-start"
      alignItems="center">
      <Grid item>
        <Tracker event="download-image" payload={{ category: 'click', value: 'rpi3' }}>
          <ImageButton device="arm-rpi3"/>
        </Tracker>
      </Grid>
      <Grid item>
        <Tracker event="download-image" payload={{ category: 'click', value: 'rpi4' }}>
          <ImageButton device="arm-rpi4"/>
        </Tracker>
      </Grid>
    </Grid>
  )
}

export const DOWNLOAD_RP_IMAGER = {
  key: 'download-rp-images',
  title: 'Download and install Raspberry Pi Imager',
  content: (
    <React.Fragment>
      <p>
        Imager is used to write the OS image you downloaded in Step 1 to your SD Card
      </p>
      <DownloadImager />
    </React.Fragment>
  )
}

export const FLASH_IMAGE = {
  key: 'flash-image',
  title: 'Flash Pantavisor image to SD Card',
  content: (
    <React.Fragment>
      <p>
        1. Launch Raspberry Pi Imager
        <br/>
        2. Choose the file you downloaded in Step 1
        <br/>
        3. Select your SD card or USB reader and click WRITE
        <br/><br/>
        <b>WARNING: This will wipe all data on this card</b>
      </p>
      <p>
        <img src={InstallGIF} alt="How to flash using Raspberry Pi imager animation" />
      </p>
      <p><b>Note:</b> The flashing process could take a while</p>
    </React.Fragment>
  )
}

export const START_USING = {
  key: 'start-using',
  title: 'Start using Pantahub One',
  content: (
    <React.Fragment>
      <p>
        Remove the SD card from your computer and <b>put the card into the Raspberry Pi</b>
        <br/>
        Plug your Raspberry Pi to <b>wired Ethernet</b> and a strong enough power source
      </p>
      <video
        autoPlay
        loop
        muted
        width="700">
        <source
          src="/app/videos/rpiconnect.webm"
          type="video/webm"
        />
        <source
          src="/app/videos/rpiconnect.m4v"
          type="video/mp4"
        />
        {`Sorry, your browser doesn't support embedded videos.`}
      </video>
      <p>
        After <b>about 1-2 minutes</b> your Pi will be up and you can connect to the WiFi hotspot:<br/>
      </p>
      <div className="network-definition" style={{ backgroundColor: grey[100], padding: '1em' }}>
        <div className="field">
          <div className="field--name">
            <Wifi />Network SSID:
          </div>
          <div className="field--value">
            <b>Pantacor-One</b>
          </div>
        </div>
        <div className="field">
          <div className="field--name">
            <VpnKey />Password:
          </div>
          <div className="field--value">
            <b>pantacorone</b>
          </div>
        </div>
      </div>
      <p>
        <b>You are all set!</b> Start installing Apps and using your Raspberry Pi.
      </p>
    </React.Fragment>
  )
}
