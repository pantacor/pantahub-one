/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState, useEffect } from 'react'
import Container from '@material-ui/core/Container'
import Grow from '@material-ui/core/Grow'
import Grid from '@material-ui/core/Grid'

import * as Steps from './steps'

import './howtoflash.scss'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

const arrayofsteps = [
  Steps.DOWNLOAD_IMAGE,
  Steps.DOWNLOAD_RP_IMAGER,
  Steps.FLASH_IMAGE,
  Steps.START_USING
]

export function HowToFlash ({ showMoreDefault }) {
  const [showMore, setShowMore] = useState(showMoreDefault)

  useEffect(() => {
    setShowMore(showMoreDefault)
  }, [showMoreDefault])

  const OnShowMore = () => {
    setShowMore(!showMore)
    window.scrollTo(0, 0)
  }

  const steps = arrayofsteps

  return (
    <div className="how-to-flash">
      <Grow
        in={showMore}>
        {showMore ? (
          <React.Fragment>
            <Container>
              <Grid
                container
                direction="row"
                justify="center"
                alignItems="center"
              >
                <Grid item md={6} xs={12}>
                  <h1 className="steps-title">{`Let's begin`}</h1>
                  {steps.map((step, index) => (
                    <section key={step.key} className="step">
                      <header className="step-header">
                        <section className="number">
                          {index + 1}
                        </section>
                        <section className="title">
                          <h1>
                            {step.title}
                          </h1>
                        </section>
                      </header>
                      <section className="step-content">
                        {step.content}
                      </section>
                    </section>
                  ))}
                </Grid>
              </Grid>
            </Container>
          </React.Fragment>
        ) : (<div>Loading...</div>)}
      </Grow>
      <TrackedButton
        event='toggle-how-to'
        payload={{ category: 'click', value: !showMore ? ' Setup a fresh Raspberry Pi ' : 'Start Managing Devices' }}
        variant="outlined"
        color="primary"
        size="medium"
        onClick={OnShowMore} >
        { !showMore ? ' Setup a fresh Raspberry Pi ' : 'Start Managing Devices' }
      </TrackedButton>
    </div>
  )
}
