/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import Form from '@rjsf/material-ui'
import { UpdateConfigOnState } from '../../../lib/markeplace.helper'
import { Box, Button } from '@material-ui/core'
import { STATUS as DEVICE_STATUS } from '../../../store/devices/reducer'
import { Track } from '../../../lib/analytics.helper'

export default function ConfigForm (props) {
  const {
    config,
    state,
    appKey,
    devicePostState
  } = props

  const data = state[`_config/${appKey}${config.file}`] || {}
  const disabled =
    props.deviceData.status === DEVICE_STATUS.DEPLOY.IN_PROGRESS

  const onSubmit = ({ formData }, event) => {
    const newState = UpdateConfigOnState(state, appKey, config, formData)
    devicePostState(
      props.deviceData.device.id,
      newState,
      `Update ${appKey} configuration file: ${config.file}`
    )
    Track(`${appKey} Submit Config`)
  }

  return (
    <Form
      disabled={disabled}
      onSubmit={onSubmit}
      formData={data}
      schema={config.schema}
      uiSchema={config.ui}
    >
      <Box marginTop={3}>
        <Button type="submit" variant="outlined" color="primary">
          Save config
        </Button>
      </Box>
    </Form>
  )
}
