/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import PropTypes from 'prop-types'
import ReactMarkdown from 'react-markdown'
import { Tabs, Tab, Box, AppBar, Typography } from '@material-ui/core'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import SwipeableViews from 'react-swipeable-views'
import MetaForm from './MetaForm'
import ConfigForm from './ConfigForm'
import Status from './Status'
import { Track } from '../../../lib/analytics.helper'

function TabPanel (props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          {children}
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
}

function a11yProps (index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`
  }
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%'
  }
}))

const TABS_LABELS = {
  README: 'Readme',
  OVERVIEW: 'Overview',
  SETTINGS: 'Settings'
}

function getTabLabels (manifest) {
  const tabs = []

  if (manifest['readme'] && manifest['readme'] !== '') {
    tabs.push(TABS_LABELS.README)
  }
  if (
    (manifest['_readme'] || manifest['_status'] || manifest['_metaconfig']) &&
    (manifest['_readme'] !== '' || (manifest['_status'] || []).length > 0 || (manifest['_metaconfig'] || []).length > 0)
  ) {
    tabs.push(TABS_LABELS.OVERVIEW)
  }
  if (manifest['_configfiles'] && manifest['_configfiles'].length > 0) {
    tabs.push(TABS_LABELS.SETTINGS)
  }

  return tabs
}

export default function BodyTabs (props) {
  const {
    appKey,
    manifest,
    userMeta,
    state,
    ...extraProps
  } = props

  const classes = useStyles()
  const theme = useTheme()
  const [value, setValue] = React.useState(0)

  const handleChange = (event, newValue) => {
    Track(`${appKey} Change Tabs`, { value: newValue })
    setValue(newValue)
  }

  const handleChangeIndex = (index) => {
    setValue(index - 1)
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicMetasatorColor="primary"
          textColor="primary"
          variant="fullWidth"
          aria-label="full width tabs"
        >
          {getTabLabels(manifest).map((label, index) => (
            <Tab key={index} label={label} {...a11yProps(index)} />
          ))}
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        {getTabLabels(manifest).map((label, index) => {
          switch (label) {
            case TABS_LABELS.README:
              return (
                <TabPanel key={index} value={value} index={index} dir={theme.direction}>
                  <ReactMarkdown source={manifest['readme']} />
                </TabPanel>
              )
            case TABS_LABELS.OVERVIEW:
              return (
                <TabPanel key={index} value={value} index={index} dir={theme.direction}>
                  <ReactMarkdown source={manifest['_readme']} />
                  {(manifest['_status'] || []).map((config, key) => (
                    <React.Fragment key={key}>
                      <Typography variant="h6" className="mb--1">
                        {config.title || ''}
                      </Typography>
                      <Status
                        deviceData={props.deviceData}
                        config={config}
                      />
                    </React.Fragment>
                  ))}
                  {(manifest['_metaconfig'] || []).map((config, key) => (
                    <MetaForm
                      appKey={appKey}
                      key={key}
                      config={config}
                      userMeta={userMeta}
                      {...extraProps}
                    />
                  ))}
                </TabPanel>
              )
            case TABS_LABELS.SETTINGS:
              return (
                <TabPanel key={index} value={value} index={index} dir={theme.direction}>
                  {(manifest['_configfiles'] || []).map((config, key) => (
                    <ConfigForm
                      key={key}
                      config={config}
                      state={state}
                      appKey={appKey}
                      {...extraProps}
                    />
                  ))}
                </TabPanel>
              )
            default:
              return null
          }
        })}
      </SwipeableViews>
    </div>
  )
}
