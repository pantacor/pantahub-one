/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import BodyTabs from './BodyTabs'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import { devicePostState, deviceSetMetadata } from '../../../store/devices/actions'
import { resolvePath, ObjectEqual } from '../../../lib/utils.helper'

import './dinamicappbody.scss'

class DinamicAppBody extends React.Component {
  shouldComponentUpdate (prevProps, prevState) {
    const userMetasEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'device.user-meta', {}),
      resolvePath(this.props.deviceData, 'device.user-meta', {})
    )

    const deviceMetasEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'device.device-meta', {}),
      resolvePath(this.props.deviceData, 'device.device-meta', {})
    )

    const stateEqual = ObjectEqual(
      resolvePath(prevProps.deviceData, 'activeStep.state', {}),
      resolvePath(this.props.deviceData, 'activeStep.state', {})
    )

    return !(deviceMetasEqual && userMetasEqual && stateEqual)
  }

  render () {
    const {
      deviceData
    } = this.props

    return (
      <Grid
        container
        spacing={4}
        style={{
          width: 'calc(100% + 44px)',
          margin: '-24px'
        }}
        alignContent="stretch"
        alignItems="stretch"
        className="dynamic-app"
      >
        <BodyTabs
          userMeta={resolvePath(deviceData, 'device.user-meta', {})}
          state={resolvePath(deviceData, 'activeStep.state', {})}
          {...this.props}
        />
      </Grid>
    )
  }
}

export default connect(
  (state) => ({ token: state.auth.token }),
  {
    devicePostState,
    deviceSetMetadata
  }
)(DinamicAppBody)
