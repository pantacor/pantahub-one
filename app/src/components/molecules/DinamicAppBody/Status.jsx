/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import {
  TableContainer,
  Table,
  TableBody,
  TableRow,
  TableCell,
  Paper
} from '@material-ui/core'
import { resolvePath } from '../../../lib/utils.helper'
import { makeStyles } from '@material-ui/core/styles'
import { NormalizeJsonToMeta } from '../../../lib/markeplace.helper'

/*
        <TableHead>
          <TableRow>
            <TableCell>Dessert (100g serving)</TableCell>
            <TableCell align="right">Calories</TableCell>
            <TableCell align="right">Fat&nbsp;(g)</TableCell>
            <TableCell align="right">Carbs&nbsp;(g)</TableCell>
            <TableCell align="right">Protein&nbsp;(g)</TableCell>
          </TableRow>
        </TableHead>
*/

const DATA_TYPES = {
  DEVICE_META: 'device-meta',
  USER_META: 'user-meta',
  CONFIG_FILE: 'device-meta',
  DEVICE_DATA: 'device'
}

const getDataByType = (deviceData, property) => {
  let data
  switch (property.type) {
    case DATA_TYPES.DEVICE_META:
      data = NormalizeJsonToMeta(resolvePath(deviceData, 'device.device-meta', {}))
      return data[property.key] || property.default
    case DATA_TYPES.USER_META:
      data = NormalizeJsonToMeta(resolvePath(deviceData, 'device.user-meta', {}))
      return data[property.key] || property.default
    case DATA_TYPES.DEVICE_DATA:
      return resolvePath(deviceData, property.key)
    default:
      return property.key
  }
}

const useStyles = makeStyles({
  table: {
    minWidth: '100%'
  }
})

export default function Status (props) {
  const classes = useStyles()
  const {
    deviceData,
    config
  } = props

  if (!config || !config.properties || config.properties.length === 0) {
    return null
  }

  const data = config.properties.map((s) => ({
    ...s,
    value: getDataByType(deviceData, s)
  }))

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableBody>
          {data.map((row) => (
            <TableRow key={row.key}>
              <TableCell component="th" scope="row">
                {row.title}
              </TableCell>
              <TableCell align="right">
                {row.value}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
