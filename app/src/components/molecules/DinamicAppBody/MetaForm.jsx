/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import Form from '@rjsf/material-ui'
import { NormalizeJsonToMeta } from '../../../lib/markeplace.helper'
import { Box, Button } from '@material-ui/core'
import SwitchWidget from '../../atoms/SwitchWidget/SwitchWidget'
import { STATUS as DEVICE_STATUS } from '../../../store/devices/reducer'
import { Track } from '../../../lib/analytics.helper'

const widgets = {
  CheckboxWidget: SwitchWidget
}

export default function MetaForm (props) {
  const {
    config,
    userMeta,
    deviceData,
    deviceSetMetadata,
    appKey
  } = props

  const disabled =
    props.deviceData.status === DEVICE_STATUS.META.IN_PROGRESS

  const data = Object.keys(userMeta).reduce((acc, key) => {
    if (key.indexOf(config['meta-key-prefix']) === 0) {
      const name = key.replace(`${config['meta-key-prefix']}.`, '')

      if (
        typeof userMeta[key] === 'string' &&
        (userMeta[key].indexOf('true') === 0 || userMeta[key].indexOf('false') === 0)
      ) {
        acc[name] = userMeta[key] === 'true'
      } else {
        acc[name] = userMeta[key]
      }
    }
    return acc
  }, {})

  const onSubmit = ({ formData }, event) => {
    const meta = NormalizeJsonToMeta(formData, config['meta-key-prefix'])
    Track(`${appKey} Submit Meta`)
    deviceSetMetadata(deviceData.device.id, 'user-meta', meta)
  }

  const onChange = ({ formData }) => {
    const meta = NormalizeJsonToMeta(formData, config['meta-key-prefix'])
    Track(`${config['meta-key-prefix']}.enabled`, { value: formData.enabled })
    deviceSetMetadata(deviceData.device.id, 'user-meta', meta)
  }

  return (
    <Form
      disabled={disabled}
      onSubmit={onSubmit}
      onChange={onChange}
      formData={data}
      schema={config.schema}
      uiSchema={config.ui}
      widgets={widgets}
      className="meta-form"

    >
      <Box marginTop={3}>
        <Button type="submit" variant="outlined" color="primary">
          Apply
        </Button>
      </Box>
    </Form>
  )
}
