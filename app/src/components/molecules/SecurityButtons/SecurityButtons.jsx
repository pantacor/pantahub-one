/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { SECURITY_OPTIONS, CLOUDFARE_OPEN_URL, CLOUDFARE_SECURE_URL, CLOUDFARE_KIDS_URL, SECURITY_OPTION_USER_META_KEY, CURRENT_STEP_NOTIFICATIONS } from '../../../lib/const'
import { ChildCare, Lock, LockOpen, MoreVert } from '@material-ui/icons'
import Grid from '@material-ui/core/Grid'
import { IconButton } from '@material-ui/core'
import { EXPANDED_TABS } from '../../../lib/devices.helper'
import { TrackedButton, Tracker } from '../../atoms/Tracker/Tracker'

const clickWithoutPropagation = (cb) => (evnt) => {
  evnt.preventDefault()
  evnt.stopPropagation()
  cb(evnt)
}

export default function SecurityButtons (props) {
  const {
    deviceData,
    currentStepNotification,
    classes,
    onChangeSecurityOptionClick,
    onExpandClick,
    biggerThanMediumScreen
  } = props
  const {
    device,
    loadingDevice
  } = deviceData

  const userMeta = device && device['user-meta'] ? device['user-meta'] : null

  const securityButtons = [
    {
      securityOption: SECURITY_OPTIONS.OPEN,
      color: classes.successOption,
      icon: <LockOpen />,
      url: CLOUDFARE_OPEN_URL
    },
    {
      securityOption: SECURITY_OPTIONS.SECURE,
      color: classes.warningOption,
      icon: <Lock />,
      url: CLOUDFARE_SECURE_URL
    },
    {
      securityOption: SECURITY_OPTIONS.KIDS,
      color: classes.dangerOption,
      icon: <ChildCare />,
      url: CLOUDFARE_KIDS_URL
    }
  ]

  const optionSelected = (userMeta, securityButton) => userMeta &&
    userMeta[SECURITY_OPTION_USER_META_KEY] &&
    userMeta[SECURITY_OPTION_USER_META_KEY] === securityButton.url

  const onClick = (securityButton) => evt => {
    evt.preventDefault()
    evt.stopPropagation()
    if (!optionSelected(userMeta, securityButton)) {
      onChangeSecurityOptionClick(securityButton.url)
    }
  }

  return (
    <React.Fragment>
      {securityButtons.map(securityButton => (
        <Grid item key={securityButton.securityOption}>
          <TrackedButton
            event="change-security-option"
            payload={{ value: securityButton.securityOption }}
            variant='contained'
            className={`${classes.button} ${!loadingDevice ? securityButton.color : classes.disabled} ${optionSelected(userMeta, securityButton) ? classes.selected : classes.notSelected}`}
            onClick={onClick(securityButton)}
            startIcon={securityButton.icon}
            size={(biggerThanMediumScreen ? 'medium' : 'small')}
            disabled={loadingDevice || currentStepNotification === CURRENT_STEP_NOTIFICATIONS.LOADING}>
            {securityButton.securityOption}
          </TrackedButton>
        </Grid>)
      )}
      <Grid item>
        <Tracker
          event="toggle-details"
        >
          <IconButton size='small' onClick={clickWithoutPropagation(onExpandClick(EXPANDED_TABS.SETTINGS))}>
            <MoreVert fontSize='small' />
          </IconButton>
        </Tracker>
      </Grid>
    </React.Fragment>
  )
}
