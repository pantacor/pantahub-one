/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import {
  SECURITY_OPTION_USER_META_KEY,
  SECURITY_OPTIONS,
  CLOUDFARE_OPEN_URL,
  CLOUDFARE_SECURE_URL,
  CLOUDFARE_KIDS_URL
} from '../../../lib/const'
// import { LockOpen, Lock, ChildCare } from '@material-ui/icons'
import Grid from '@material-ui/core/Grid'

import './internetfilter.scss'
import { ButtonBase } from '@material-ui/core'
import Unlock from '../../../assets/images/unlock.svg'
import Safe from '../../../assets/images/safe.svg'
import Family from '../../../assets/images/family.svg'
import { Track } from '../../../lib/analytics.helper'

const securityButtons = [
  {
    name: SECURITY_OPTIONS.OPEN,
    color: 'open',
    text: 'Open, unrestricted use',
    url: CLOUDFARE_OPEN_URL,
    Icon (props) {
      return (
        <img
          src={Unlock}
          alt='Allow all content'
          {...props} />
      )
    }
  },
  {
    name: SECURITY_OPTIONS.SECURE,
    color: 'secure',
    text: 'Block dangerous Malware sites',
    url: CLOUDFARE_SECURE_URL,
    Icon (props) {
      return (
        <img
          src={Safe}
          alt='Safe navigation'
          {...props} />
      )
    }
  },
  {
    name: SECURITY_OPTIONS.KIDS,
    color: 'kids',
    text: 'Block Malware and Adult Content',
    tooltip: '',
    url: CLOUDFARE_KIDS_URL,
    Icon (props) {
      return (
        <img
          src={Family}
          alt='Kids friendly'
          {...props} />
      )
    }
  }
]

export default function InternetFilters (props) {
  const {
    classes,
    biggerThanMediumScreen,
    deviceSetMetadata,
    deviceData
  } = props
  const {
    device,
    loadingDevice
  } = deviceData

  const userMeta = device && device['user-meta'] ? device['user-meta'] : null

  const optionSelected = (userMeta, option) => {
    return userMeta &&
      userMeta[SECURITY_OPTION_USER_META_KEY] &&
      userMeta[SECURITY_OPTION_USER_META_KEY] === option.url
  }

  const onChangeSecurityOptionClick = (newUrl) => {
    deviceSetMetadata(device.id, 'user-meta', { [SECURITY_OPTION_USER_META_KEY]: newUrl })
  }

  const onClick = (securityButton) => evt => {
    evt.preventDefault()
    evt.stopPropagation()
    if (!optionSelected(userMeta, securityButton)) {
      Track(`Internet Filter`, { value: securityButton.color })
      onChangeSecurityOptionClick(securityButton.url)
    }
  }

  const getClasess = (option) =>
    `internet-filters ${option.color} ${!optionSelected(userMeta, option) ? 'disabled' : 'enabled'}`

  return (
    <Grid container className={classes.deviceBody}>
      <Grid
        item
        container
        spacing={2}
        direction="row"
        justify="space-around"
        alignItems="center"
        style={{ marginTop: '0px' }}
        className={(biggerThanMediumScreen ? classes.deviceBodyFormLarge : classes.deviceBodyFormSmall)}
      >
        {securityButtons.map((option) => (
          <Grid
            key={option.name}
            item
            md={4}
            xs={12}
          >
            <ButtonBase
              variant="outlined"
              disabled={loadingDevice}
              className={getClasess(option)}
              onClick={onClick(option)}
            >
              <div className="button-inner">
                <option.Icon
                  className="icon"
                />
                <span>{option.text}</span>
              </div>
            </ButtonBase>
          </Grid>
        ))}
      </Grid>
    </Grid>
  )
}
