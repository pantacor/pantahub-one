/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import Loading from '../../atoms/Loading/Loading'
import relativeTime from 'dayjs/plugin/relativeTime'
import dayjs from 'dayjs'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import { resolvePath } from '../../../lib/utils.helper'
import { BorderLinearProgress } from '../../atoms/BorderLinearProgress/BorderLinearProgress'
import { Typography } from '@material-ui/core'

dayjs.extend(relativeTime)

const defaultTotal = {
  object_name: 'total',
  object_id: 'none',
  total_size: 0,
  start_time: 0,
  current_time: 0,
  total_downloaded: 0
}

const getDownloadData = (download = defaultTotal) => {
  const currentTime = dayjs()
  const startTime = dayjs.unix(download.start_time)
  const downloaded = download.total_downloaded / (1024 * 1024)
  const size = download.total_size / (1024 * 1024)
  const timepassed = (
    download.current_time > 0 &&
    download.start_time > 0
  )
    ? currentTime.diff(startTime, 'second')
    : 0

  const downloadSpeed = timepassed <= 0
    ? 0
    : downloaded / timepassed

  const eta = downloadSpeed > 0
    ? (size - downloaded) / downloadSpeed
    : -1

  return {
    ...download,
    eta,
    downloaded: downloaded,
    size: size,
    speed: downloadSpeed
  }
}

function Eta ({ eta }) {
  if (eta === -1) {
    return (
      <Typography variant="body2">
        Time remaining: unknown
      </Typography>
    )
  }

  const now = dayjs()
  const finished = now.add(eta, 'second')
  // onMinutes = (onMinutes / 60).toFixed(2)
  //       Time remaining: {onMinutes < 1 ? eta.toFixed(2) : onMinutes} {onMinutes < 1 ? 'seconds' : 'minutes'}
  return (
    <Typography variant="body2">
      Time remaining: {now.to(finished, true)}
    </Typography>
  )
}

function Speed ({ speed }) {
  return (
    <Typography variant="body2">
      Download Speed: {speed.toFixed(2)} MB/s
    </Typography>
  )
}

function Progress ({ downloaded, size }) {
  const progress = ((downloaded / size) * 100).toFixed(2)
  const text = `Downloaded ${downloaded.toFixed(2)} MB of ${size.toFixed(2)} MB`
  return (
    <div className="progress-with-label">
      <span className="progress-with-label--label">
        {text}
      </span>
      <BorderLinearProgress
        title={text}
        variant="determinate"
        value={progress}
      />
    </div>
  )
}

function Message ({ progress }) {
  const total = getDownloadData(resolvePath(progress, 'downloads.total', defaultTotal))

  if (progress.status !== 'DOWNLOADING') {
    return (
      <div>
        {progress['status-msg']}
      </div>
    )
  }

  if (total.eta === -1) {
    return (
      <div>
        Starting download process
      </div>
    )
  }

  return (
    <ul className="list-no-bullets">
      <li style={{ paddingTop: '1em', paddingBottom: '1em' }}>
        <Progress {...total} />
      </li>
      <li><Eta {...total} /></li>
      <li><Speed {...total} /> </li>
    </ul>
  )
}

function Title ({ progress }) {
  if (progress.status !== 'DOWNLOADING') {
    return (
      <div>
        Applying changes
          - {resolvePath(progress, 'progress', 0)}%
      </div>
    )
  }

  return (
    <div>
      Applying changes
    </div>
  )
}

export default function LoadingModal (props) {
  const isMobile = useMediaQuery('(max-width:560px)')
  return (
    <div className={`${props.classes.deviceBodyModal}`}>
      <Loading divClassName={`${props.classes.deviceBodyLoadingWrapper}`} />
      <div style={{ width: isMobile ? '90%' : '50%' }}>
        <Title progress={resolvePath(props, 'deviceData.currentStep.progress', {})} />
        <Message progress={resolvePath(props, 'deviceData.currentStep.progress', {})} />
      </div>
    </div>
  )
}
