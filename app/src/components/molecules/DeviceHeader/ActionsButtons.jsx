/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { TrackerWrapper } from '../../atoms/Tracker/Tracker'
import { clickWithoutPropagation } from '../../../lib/utils.helper'
import ToggleButton from '@material-ui/lab/ToggleButton'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'

import './actionsbuttons.scss'

const TrackedButtons = TrackerWrapper(ToggleButton)

const isFunction = (obj) =>
  !!(obj && obj.constructor && obj.call && obj.apply)

export default function ActionButtons (props) {
  const {
    onExpandClick,
    expanded,
    options
  } = props

  return (
    <ToggleButtonGroup
      value={expanded}
      exclusive
      className="actions-buttons"
      aria-label="contained primary button group"
      size='small'
    >
      {options.map((option, index) => (
        <TrackedButtons
          key={index}
          event={option.eventName}
          payload={{ value: option.text }}
          value={option.expanedOption}
          style={{ flex: '1 0 auto' }}
          onClick={clickWithoutPropagation(onExpandClick(option.expanedOption))}
        >
          {isFunction(option.icon)
            ? option.icon(props)
            : option.icon
          }
          <span dangerouslySetInnerHTML={{ __html: option.text }}></span>
        </TrackedButtons>
      ))}
    </ToggleButtonGroup>
  )
}
