/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import Grid from '@material-ui/core/Grid'
import { Tooltip, Zoom } from '@material-ui/core'
import SettingsIcon from '@material-ui/icons/Settings'

import NotificationIcon from '../NotificationIcon/NotificationIcon'
import EditableElement from '../../atoms/Editable/Editable'
import ActionButtons from './ActionsButtons'
import NotificationOrInstallation from './NotificationOrInstallation'
import { CURRENT_STEP_NOTIFICATIONS } from '../../../lib/const'
import { EXPANDED_TABS } from '../../../lib/devices.helper'
import InfoIcon from '@material-ui/icons/Info'

const MenuOptions = [
  {
    text: null,
    eventName: 'toggle_settings',
    color: 'default',
    icon: function Icon (props) {
      return props.deviceData.needUpdate ? <UpdateNeedIcon /> : <SettingsIcon />
    },
    expanedOption: EXPANDED_TABS.SETTINGS_MANAGER
  }
]

const iconStyle = {
  position: 'absolute',
  zIndex: 200,
  top: '-10px',
  right: '-10px',
  fontSize: '20px'
}

function UpdateNeedIcon () {
  return (
    <Tooltip title="System update available" placement="top">
      <React.Fragment>
        <SettingsIcon />
        <InfoIcon
          style={iconStyle}
          color="secondary"
        />
      </React.Fragment>
    </Tooltip>
  )
}

export default function DeviceHeader (props) {
  const {
    deviceData,
    classes,
    installPlatform,
    biggerThanMediumScreen,
    expanded,
    currentStepNotification,
    devicePatch
  } = props
  const {
    device,
    loadingDevice,
    platformInstalled,
    platformSupported,
    isOnline
  } = deviceData

  const userMeta = (device && device['user-meta'] ? device['user-meta'] : null)

  const deviceText = (device) => device.nick || device.id

  const onInstallClick = () => {
    if (installPlatform && device && device.id) {
      installPlatform(device.id)
    }
  }

  const nick = deviceText(device)

  const onEditNameSave = (nick) => {
    devicePatch(device.id, { nick })
  }

  const headerClasses = (platformSupported && !loadingDevice
    ? classes.deviceHeaderEnabled : classes.deviceHeaderDisabled) +
    ' device-header ' +
    (biggerThanMediumScreen ? classes.deviceHeaderLarge : classes.deviceHeaderSmall) +
    ' ' +
    (isOnline ? 'device-online' : 'device-offline')

  const margins = biggerThanMediumScreen ? '0' : '0.5em'
  const moreContent =
    !platformInstalled ||
    currentStepNotification !== CURRENT_STEP_NOTIFICATIONS.NONE

  return (
    <Grid
      container
      alignItems="center"
      justify="flex-end"
      className={headerClasses}
    >
      <Tooltip
        TransitionComponent={Zoom}
        placement="left"
        classes={{ tooltip: classes.textLarge }}
        title={isOnline ? 'Online' : 'Offline'}
      >
        <div className="device-status-bar"></div>
      </Tooltip>
      <Grid
        container
        item
        md={5}
        xs={moreContent ? 12 : 10}
        justify="flex-start"
        alignItems="center"
      >
        <Grid
          item
          className={`${classes.deviceName} editable-area`}
          style={{ pointerEvents: 'unset', cursor: 'default' }}
        >
          {platformInstalled && !loadingDevice
            ? (
              <EditableElement
                el='span'
                name='device-nick'
                value={nick}
                editingHandler={null}
                saving={deviceData.loadingDevice}
                slugify={true}
                truncate={true}
                saveHandler={onEditNameSave}
                buttonClassName={classes.deviceEditableButtons}
              />
            )
            : nick
          }
        </Grid>
        <NotificationIcon
          classes={classes}
          status={currentStepNotification}
          expanded={expanded}
        />
      </Grid>
      <Grid
        container
        item
        md={7}
        xs={moreContent ? 12 : 2}
        justify="flex-end"
        alignItems="center"
        style={{ marginTop: margins, marginBottom: margins, marginLeft: 0 }}
        spacing={1}
      >
        <Grid item xs={10}>
          <NotificationOrInstallation
            currentStep={deviceData.currentStep}
            activeStep={deviceData.activeStep}
            platformSupported={platformSupported}
            platformInstalled={platformInstalled}
            classes={classes}
            currentStepNotification={currentStepNotification}
            onInstallClick={onInstallClick}
          />
        </Grid>
        {platformInstalled && (
          <Grid item md={2} xs={moreContent ? 2 : 12 }>
            <ActionButtons
              {...props}
              options={MenuOptions}
              userMeta={userMeta}
            />
          </Grid>
        )}
      </Grid>
    </Grid>
  )
}
