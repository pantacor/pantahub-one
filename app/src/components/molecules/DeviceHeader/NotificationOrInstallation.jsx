/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useEffect, useState } from 'react'
import { CloudDownload } from '@material-ui/icons'

import { CURRENT_STEP_NOTIFICATIONS } from '../../../lib/const'
import { resolvePath } from '../../../lib/utils.helper'
import { BorderLinearProgress } from '../../atoms/BorderLinearProgress/BorderLinearProgress'
import { TrackedButton } from '../../atoms/Tracker/Tracker'

export default function NotificationOrInstallation (props) {
  const {
    platformSupported,
    onInstallClick,
    classes,
    currentStep,
    platformInstalled,
    currentStepNotification
  } = props
  const [error, setError] = useState({ count: 0, show: false })

  useEffect(() => {
    if (error.count === 0 && currentStepNotification === CURRENT_STEP_NOTIFICATIONS.ERROR) {
      setError({ count: 1, show: true })
      setTimeout(() => {
        setError({ count: 1, show: false })
      }, 2000)
    }
  }, [error, currentStepNotification])

  if (!platformInstalled) {
    return (
      <TrackedButton
        event="install-families"
        variant='contained'
        className={`${classes.button} ${classes.success} ${classes.fullWidth}`}
        onClick={onInstallClick}
        disabled={currentStepNotification === CURRENT_STEP_NOTIFICATIONS.LOADING}
        startIcon={<CloudDownload />}>
        Install Pantacor One
      </TrackedButton>
    )
  }

  if (!error.show && currentStepNotification === CURRENT_STEP_NOTIFICATIONS.ERROR) {
    return null
  }

  if (!platformSupported) {
    return (
      <TrackedButton
        event="unsuported-device"
        variant='contained'
        className={`${classes.button} ${classes.disabled} ${classes.fullWidth}`}
        disabled>
        Unsupported Device
      </TrackedButton>
    )
  }

  if (error.show) {
    return (
      <div className="progress-with-label">
        <span className="progress-with-label--label">
          Error applying changes
        </span>
        <BorderLinearProgress
          variant="determinate"
          value={100}
        />
      </div>
    )
  }

  if (currentStepNotification !== CURRENT_STEP_NOTIFICATIONS.NONE) {
    let text = resolvePath(currentStep, 'progress.status', '') === 'DOWNLOADING'
      ? 'Downloading objects'
      : resolvePath(currentStep, 'progress.status-msg', '')

    text = text === '' ? 'Waiting for device...' : text

    return (
      <div className="progress-with-label">
        <span className="progress-with-label--label">
          {text}
        </span>
        <BorderLinearProgress
          title={text}
          variant="determinate"
          value={resolvePath(currentStep, 'progress.progress', 0)}
        />
      </div>
    )
  }

  return null
}
