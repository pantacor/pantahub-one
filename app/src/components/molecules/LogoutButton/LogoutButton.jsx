/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { connect } from 'react-redux'
import { Logout } from '../../../store/auth/actions'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles(theme => ({
  fab: {
    'text-transform': 'inherit !important',
    'box-shadow': 'initial !important'
  }
}))

export function LogoutButton (props) {
  const { token, nick, type, Logout, ...rest } = props
  const classes = useStyles()

  if (token === null) {
    return null
  }

  return (
    <Button
      className={classes.fab}
      color='default'
      onClick={Logout}
      size='small'
      {...rest}
    >
      Logout from { type !== 'SESSION' ? nick : 'Anonymous' }
    </Button>
  )
}

export default connect(state => ({
  token: state.auth.token,
  nick: state.auth.nick,
  type: state.auth.type
}), {
  Logout
})(LogoutButton)
