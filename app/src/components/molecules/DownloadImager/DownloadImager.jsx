/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown'
import ClickAwayListener from '@material-ui/core/ClickAwayListener'
import Grow from '@material-ui/core/Grow'
import Paper from '@material-ui/core/Paper'
import Popper from '@material-ui/core/Popper'
import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import { Track } from '../../../lib/analytics.helper'

const options = [
  {
    name: 'Select your OS'
  },
  {
    name: 'Download for Windows',
    link: 'https://downloads.raspberrypi.org/imager/imager_latest.exe'
  },
  {
    name: 'Download for Mac OS',
    link: 'https://downloads.raspberrypi.org/imager/imager_latest.dmg'
  },
  {
    name: 'Download for Linux x64 (AppImage)',
    link: 'https://bit.ly/2OqU3vO',
    extra: 'To run, download and extract the Zip file and double-click on the extracted file.'
  }
]

function getOS () {
  const userAgent = navigator.userAgent.toLowerCase()
  if (userAgent.indexOf('win') >= 0) return 'windows'
  if (userAgent.indexOf('mac') >= 0) return 'macos'
  if (userAgent.indexOf('linux') >= 0) return 'linux'
  return 'Unknown OS'
}

function defaultSelected () {
  switch (getOS()) {
    case 'windows':
      return 1
    case 'macos':
      return 2
    case 'linux':
      return 3
    default:
      return 0
  }
}

export default function DownloadImager () {
  const [open, setOpen] = React.useState(false)
  const anchorRef = React.useRef(null)
  const [selectedIndex, setSelectedIndex] = React.useState(defaultSelected())

  const handleClick = () => {
    console.info(`You clicked ${options[selectedIndex]}`)
    if (options[selectedIndex].link) {
      Track('download-imager', { value: options[selectedIndex].name })
      window.open(options[selectedIndex].link)
    } else {
      handleToggle()
    }
  }

  const handleMenuItemClick = (event, index) => {
    setSelectedIndex(index)
    setOpen(false)
    window.open(options[index].link)
  }

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen)
  }

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return
    }

    setOpen(false)
  }

  return (
    <Grid container direction="column">
      <Grid item xs={12}>
        <ButtonGroup variant="outlined" color="primary" ref={anchorRef} aria-label="split button">
          <Button onClick={handleClick}>{options[selectedIndex].name}</Button>
          <Button
            color="primary"
            size="small"
            aria-controls={open ? 'split-button-menu' : undefined}
            aria-expanded={open ? 'true' : undefined}
            aria-label="select merge strategy"
            aria-haspopup="menu"
            onClick={handleToggle}
          >
            <ArrowDropDownIcon />
          </Button>
        </ButtonGroup>
        <div style={{ visibility: !options[selectedIndex].extra ? 'hidden' : '' }}>
          <br/>
          {options[selectedIndex].extra ? options[selectedIndex].extra : ''}
        </div>
        <Popper
          open={open}
          anchorEl={anchorRef.current}
          role={undefined}
          transition
          disablePortal
        >
          {({ TransitionProps, placement }) => (
            <Grow
              {...TransitionProps}
              style={{
                transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'
              }}
            >
              <Paper
                elevation={3}
                variant="outlined"
              >
                <ClickAwayListener onClickAway={handleClose}>
                  <MenuList id="split-button-menu">
                    {options.map((option, index) => (
                      <MenuItem
                        key={option.name}
                        disabled={index === 0}
                        selected={index === selectedIndex}
                        onClick={(event) => handleMenuItemClick(event, index)}
                      >
                        {option.name}
                      </MenuItem>
                    ))}
                  </MenuList>
                </ClickAwayListener>
              </Paper>
            </Grow>
          )}
        </Popper>
      </Grid>
    </Grid>
  )
}
