/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState } from 'react'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Grid from '@material-ui/core/Grid'
import { BlockSwitch } from '../../atoms/BlockSwitch/BlockSwitch'
import { GameFilters } from '../../../lib/gamesfilter.helper'
import { ComingSoonWrapper } from '../../atoms/ComingSoonWrapper/ComingSoonWrapper'
import { Track } from '../../../lib/analytics.helper'

const checkMeta = (deviceData, value) => {
  const userMeta = deviceData.device['user-meta']
  if (!userMeta) {
    return false
  }
  return userMeta['dns-extra.conf'] ? userMeta['dns-extra.conf'].includes(value) : false
}

export function Form (props) {
  const {
    classes,
    biggerThanMediumScreen,
    setFilter,
    deviceData
  } = props

  const [state, setState] = useState(() => GameFilters.reduce((acc, game) => {
    return {
      ...acc,
      [game.name]: checkMeta(deviceData, game.name)
    }
  }, {}))

  const handleChange = (event) => {
    const newState = {
      ...state,
      [event.target.name]: event.target.checked
    }
    const filtersValue = Object.keys(newState).map((key) => {
      return newState[key] ? key : undefined
    })

    Track(`Games Filter`, { value: filtersValue })
    setFilter(filtersValue)
    setState(newState)
  }

  return (
    <Grid
      container
      item
      xs={12}
      className={(biggerThanMediumScreen ? classes.deviceBodyFormLarge : classes.deviceBodyFormSmall)}
    >
      {GameFilters.map((game) => (
        <GameFilter
          key={game.name}
          classes={classes}
          handleChange={handleChange}
          checked={state[game.name]}
          game={game}
        />
      ))}
    </Grid>
  )
}

function GameFilter (props) {
  const {
    game,
    classes,
    handleChange,
    checked
  } = props

  return (
    <Grid
      key={game.name}
      item
      md={6}
      xs={12}
      className={classes.deviceBodyFormBlock}
    >
      <Grid container spacing={2}>
        <Grid item md={7} xs={4}>
          <img src={game.logo} height='30px' alt={game.title} style={{ maxWidth: '100%' }} />
        </Grid>
        <Grid item md={5} xs={8}>
          <ComingSoonWrapper comingsoon={game.comingsoon}>
            <FormGroup>
              <FormControlLabel
                disabled={game.comingsoon}
                control={
                  <BlockSwitch
                    checked={checked}
                    onChange={handleChange}
                    name={game.name}
                    style={{ marginRight: '20px' }}
                  />
                }
                label={checked ? 'Blocked' : 'Allowed'}
              />
            </FormGroup>
          </ComingSoonWrapper>
        </Grid>
      </Grid>
    </Grid>
  )
}
