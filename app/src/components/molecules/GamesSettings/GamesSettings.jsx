/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import Grid from '@material-ui/core/Grid'
import { BuildDnsExtraConf } from '../../../lib/gamesfilter.helper'
import { Form } from './Form'

export default function GamesSettings (props) {
  const {
    deviceData,
    classes,
    deviceSetMetadata,
    biggerThanMediumScreen
  } = props

  const setFilter = (values) => {
    deviceSetMetadata(deviceData.device.id, 'user-meta', { 'dns-extra.conf': BuildDnsExtraConf(values) })
  }

  return (
    <Grid
      container
      item
      xs={12}
      md={12}
      alignItems="center"
      className={classes.deviceBody}
    >
      <Form
        classes={classes}
        biggerThanMediumScreen={biggerThanMediumScreen}
        deviceData={deviceData}
        setFilter={setFilter}
      />
    </Grid>
  )
}
