/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { connect } from 'react-redux'
import Button from '@material-ui/core/Button'
import { DownloadImageWithAuth } from '../../../store/auth/actions.js'

function ImageButton ({ token, device, onDownload }) {
  if (token === null) {
    return null
  }

  device = device.toLowerCase()
  const revision = device.match(/\d/)

  const Download = (event) => {
    event.preventDefault()
    onDownload(device)
  }

  return (
    <Button
      variant="outlined"
      color="primary"
      size="medium"
      onClick={Download}
    >
      Download Raspberry Pi {revision} Image
    </Button>
  )
}

export default connect(null, { onDownload: DownloadImageWithAuth })(ImageButton)
