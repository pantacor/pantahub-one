/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { PureComponent } from 'react'

import Button from '@material-ui/core/Button'

import isFunction from 'lodash.isfunction'

function placeCaretAtEnd (el) {
  el.focus()
  if (
    typeof window.getSelection !== 'undefined' &&
    typeof document.createRange !== 'undefined'
  ) {
    var range = document.createRange()
    range.selectNodeContents(el)
    range.collapse(false)
    var sel = window.getSelection()
    sel.removeAllRanges()
    sel.addRange(range)
  } else if (typeof document.body.createTextRange !== 'undefined') {
    var textRange = document.body.createTextRange()
    textRange.moveToElementText(el)
    textRange.collapse(false)
    textRange.select()
  }
}

function slugify (str, separator = '-') {
  return str
    .replace(/[^a-z0-9 +_-]/g, '') // remove invalid chars
    .replace(/\s+/g, '-') // collapse whitespace and replace by -
    .replace(/-+/g, '-') // collapse dashes
    .replace(/_+/g, '_') // collapse dashes
    .replace(/^-+/, '') // trim - from start of text
    .replace(/-+$/, '') // trim - from end of text
    .replace(/-/g, separator)
}

class EditableElement extends PureComponent {
  constructor (props) {
    super(props)
    this.input = React.createRef()
  }

  state = { editing: false, dirty: false, saving: false };

  componentDidMount () {
    const { value, saving } = this.props
    if (value === this.input.current.innerText && saving !== 'inprogr') { this.setState({ dirty: false }) }
  }

  componentDidUpdate () {
    const { value } = this.props
    const { editing, dirty } = this.state
    if (!editing && !dirty) this.input.current.innerText = value
  }

  startEditing = () => {
    const { editingHandler } = this.props
    this.setState({ editing: true })
    if (editingHandler) editingHandler(true)
    setTimeout(() => placeCaretAtEnd(this.input.current), 350)
  }

  cancelEditing = () => {
    const { value } = this.props
    this.input.current.classList.remove('dirty')
    this.input.current.innerText = value
    this.finishEditing()
  }

  finishEditing = () => {
    const { editigHandler } = this.props
    const { editing } = this.state
    if (editing) this.setState({ editing: false })
    if (isFunction(editigHandler)) editigHandler(false)
  }

  updateValue = (str) => {
    str = str.trim().toLowerCase().replace(/(\r\n|\n|\r)/gm, '').replace(/<br>/g, '')
    return this.props.slugify ? slugify(str) : str
  }

  onInput = () => {
    const { onChange } = this.props
    const editedValue = (this.input.current || {}).innerText || ''
    this.setState({ dirty: true })
    if (isFunction(onChange)) onChange(this.updateValue(editedValue))
  }

  onEnter = (event) => {
    switch (event.key) {
      case 'Enter':
        return this.state.editing && this.save()
      case 'Escape':
        return this.state.editing && this.cancelEditing()
      default:
    }
  }

  save = () => {
    const { saveHandler, value } = this.props
    const editedValue = (this.input.current || {}).innerText
    if (value !== editedValue) saveHandler(this.updateValue(editedValue))
    this.finishEditing()
  }

  onEditHandler = () => {
    const { editing } = this.state
    if (editing) this.save()
    else this.startEditing()
  }

  textValue = () => {
    const {
      value,
      truncate = false,
      size = 30
    } = this.props
    const { editing } = this.state
    if (!value) {
      return ''
    }
    return truncate && !editing && value.length > size
      ? value.substring(0, size) + ' ...'
      : value // + ' '
  }

  render () {
    const {
      el,
      name,
      saving,
      className,
      editableClassName,
      buttonClassName
    } = this.props

    const TagElement = el
    const saveInProgr = saving === 'inprogr'

    const { editing } = this.state
    const editText = editing
      ? 'save'
      : saveInProgr
        ? 'save'
        : 'edit'

    return (
      <React.Fragment>
        <TagElement
          name={name}
          className={`editable ${className || ''}`}
        >
          <span
            ref={this.input}
            className={`editable-inner ${editableClassName || ''}`}
            onInput={this.onInput}
            onKeyDown={this.onEnter}
            contentEditable={!saveInProgr && editing}
          >
            {this.textValue()}
          </span>
        </TagElement>
        <Button
          size='small'
          onClick={this.onEditHandler}
          className={`${buttonClassName || ''}`}>
          {editText}
        </Button>
        {editing ? (
          <Button
            size='small'
            className={`${buttonClassName || ''}`}
            onClick={this.cancelEditing}>
            cancel
          </Button>
        ) : null}
      </React.Fragment>
    )
  }
}

export default EditableElement
