/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { makeStyles, colors } from '@material-ui/core'
import { connect } from 'react-redux'

import { LoginAnonimusly } from '../../../store/auth/actions'
import { TrackedButton } from '../Tracker/Tracker'

export const useStyles = makeStyles(theme => ({
  logo: {
    padding: theme.spacing(5)
  },
  signInButton: {
    width: '219px',
    height: '35px',
    'border-radius': '3px',
    'background-color': colors.grey[500],
    'margin': theme.spacing(1.5),
    padding: theme.spacing(0, 0, 0, 0),
    '&:hover': {
      'background-color': colors.grey[500],
      opacity: 0.7
    }
  },
  signInText: {
    'font-family': 'SFPTMedium',
    'font-weight': 500,
    'text-transform': 'initial',
    'font-size': '15px',
    'letter-spacing': '0px',
    'line-height': '16px',
    color: '#ffffff',
    padding: theme.spacing(0, 0.5, 0, 0)
  }
}))

function AnonimousLoginButton ({ LoginAnonimusly }) {
  const classes = useStyles()

  return (
    <TrackedButton
      event="incongnito"
      onClick={LoginAnonimusly}
      className={classes.signInButton}
    >
      <span className={classes.signInText}>
        Continue as incognito
      </span>
    </TrackedButton>
  )
}

export default connect(null, { LoginAnonimusly })(AnonimousLoginButton)
