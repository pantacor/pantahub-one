/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'

import FormControlLabel from '@material-ui/core/FormControlLabel'

import { WidgetProps, utils } from '@rjsf/core'
import { BlockSwitchInv } from '../BlockSwitch/BlockSwitch'

const { schemaRequiresTrueValue } = utils

const SwitchWidget = (props: WidgetProps) => {
  const {
    schema,
    id,
    value,
    disabled,
    readonly,
    label,
    autofocus,
    onChange,
    onBlur,
    onFocus
  } = props

  // Because an unchecked checkbox will cause html5 validation to fail, only add
  // the 'required' attribute if the field value must be 'true', due to the
  // 'const' or 'enum' keywords
  const required = schemaRequiresTrueValue(schema)

  const _onChange = (_, checked: boolean) => onChange(checked)
  const _onBlur = ({ target: { value } }) => onBlur(id, value)
  const _onFocus = ({ target: { value } }) => onFocus(id, value)
  const _checked = typeof value === 'undefined' ? false : value
  const _label = label && label !== ''
    ? `${label} - ${_checked ? 'enabled' : 'disabled'}`
    : _checked ? 'enabled' : 'disabled'

  return (
    <FormControlLabel
      control={
        <BlockSwitchInv
          id={id}
          checked={_checked}
          required={required}
          disabled={disabled || readonly}
          autoFocus={autofocus}
          onChange={_onChange}
          onBlur={_onBlur}
          onFocus={_onFocus}
        />
      }
      label={_label}
    />
  )
}

export default SwitchWidget
