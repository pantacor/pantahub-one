/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { GetLocalDeviceImageUrl } from '../../../lib/localdevices.helper'

export function TestImageUrl (url) {
  return new Promise((resolve, reject) => {
    var image = new window.Image(1, 1)
    image.addEventListener('load', resolve)
    image.addEventListener('error', reject)
    image.src = url
  })
}

export default class IfConnectedToRouter extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isConnected: false
    }
  }

  componentDidMount = () => {
    TestImageUrl(GetLocalDeviceImageUrl(this.props.ip, this.props.id))
      .then(() => {
        this.setState(() => ({
          isConnected: true
        }))
      })
      .catch(() => {
        this.setState(() => ({
          isConnected: false
        }))
      })
  }

  render () {
    if (!this.state.isConnected) {
      const NoConnected = this.props.NoConnected
        ? this.props.NoConnected
        : () => null
      return (
        <NoConnected />
      )
    }

    return (
      <React.Fragment>
        {this.props.children}
      </React.Fragment>
    )
  }
}
