/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React from 'react'
import { makeStyles } from '@material-ui/core'
import { connect } from 'react-redux'

import { LoginWith } from '../../../store/auth/actions'
import { TrackedButton } from '../Tracker/Tracker'

export const useStyles = makeStyles(theme => ({
  logo: {
    padding: theme.spacing(5)
  }
}))

export function PhLoginButton (props) {
  const classes = useStyles()
  const {
    text = 'Sign in with',
    event = 'signin',
    noImage = false,
    LoginWith,
    ...rest
  } = props

  const onClick = () => {
    LoginWith('remote')
  }

  return (
    <TrackedButton
      event={event}
      onClick={onClick}
      color="primary"
      {...rest}
    >
      <span className={classes.signInText}>
        {text}
      </span>
      {!noImage && (
        <img
          src={process.env.PUBLIC_URL + '/pantacor.png'}
          alt='Pantacor'
          height='25px'
        />
      )}
    </TrackedButton>
  )
}

export default connect(
  null,
  {
    LoginWith
  }
)(PhLoginButton)
