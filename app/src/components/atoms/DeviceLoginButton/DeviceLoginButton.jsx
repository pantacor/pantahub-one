/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useState } from 'react'
import { connect } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'

import { TrackedButton } from '../Tracker/Tracker'
import { LoginWith } from '../../../store/auth/actions'
import CircularProgress from '@material-ui/core/CircularProgress'

export const useStyles = makeStyles(theme => ({
  logo: {
    padding: theme.spacing(5)
  }
}))

export function DeviceLoginButton (props) {
  const {
    LoginWith,
    deviceip,
    text = `Sign-in to this device`,
    ...rest
  } = props
  const classes = useStyles()
  const [loading,
    setLoading] = useState(false)

  const onClick = () => {
    LoginWith('local', deviceip)
    setLoading(true)
  }

  return (
    <TrackedButton
      event="device-signin"
      onClick={onClick}
      variant="outlined"
      color="primary"
      {...rest}
    >
      <span className={classes.signInText}>
        {loading ? (
          <CircularProgress
            color="secondary"
            size={16}
            thickness={6}
          />
        ) : (
          text
        )}
      </span>
    </TrackedButton>
  )
}

export default connect(
  null,
  {
    LoginWith
  }
)(DeviceLoginButton)
