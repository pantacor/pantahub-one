/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import Link from '@material-ui/core/Link'
import Container from '@material-ui/core/Container'
import LogoutButton from '../molecules/LogoutButton/LogoutButton'
import { ThemeProviderLoaded } from '../../lib/theme.helper'
import TemporalAlert from '../atoms/TemporalAlert/TemporalAlert'
import { clearError } from '../../store/general-errors/actions'
import useMediaQuery from '@material-ui/core/useMediaQuery'
import PhLoginButton from '../atoms/PhLoginButton/PhLoginButton'
import { getBaseDevice } from '../../store/base-system/actions'
import Grid from '@material-ui/core/Grid'
import Footer from '../molecules/Footer/Footer'

import { Track } from '../../lib/analytics.helper'

export function Copyright () {
  return (
    <Typography variant='body2' color='textSecondary' align='center'>
      {'1.1.1.1 for Families is a trademark of Cloudflare, Inc'}
      <br />
      {'Copyright © '}
      <Link color='inherit' href='https://hub.pantacor.com'>
        Pantacor
      </Link>{' '}
      {new Date().getFullYear()}
    </Typography>
  )
}

function MainLayout ({ clearError, getBaseDevice, status, username, children, isLogged, error }) {
  const mobile = useMediaQuery('(max-width:560px)') // Material UI medium size

  useEffect(() => {
    if (isLogged) {
      Track('app-loading-with-user', { screen_name: 'Main', username })
      if (window.mixpanel && window.mixpanel.identify) {
        window.mixpanel.identify(username)
      }
    } else {
      Track('app-loading-without-user', { screen_name: 'Main' })
    }
  }, [isLogged, username])

  return (
    <ThemeProviderLoaded>
      <CssBaseline />
      {error && error.message && (
        <TemporalAlert onClose={clearError} variant="filled" severity="error">
          {error.message} - {error.code}
        </TemporalAlert>
      )}
      <header
        className="main-header diagonal-bg primary with-shadow"
        style={{ padding: mobile ? '2em 0px 0.5em' : '2em 0px 2em' }}
      >
        <Container maxWidth='lg'>
          <Grid container justify="center">
            <Grid item xs={12} sm={10} style={{ textAlign: 'left' }}>
              <a href="https://one.pantacor.com">
                <img
                  style={{ height: '38px' }}
                  src={process.env.PUBLIC_URL + '/images/pantacor_white.png'}
                  alt='PantacorOne'
                />
              </a>
            </Grid>
            <Grid item sm={2} xs={12}>
              {isLogged ? (
                <LogoutButton
                  variant="contained"
                  color="primary"
                />
              ) : (
                <React.Fragment>
                  <PhLoginButton
                    event="login"
                    variant="contained"
                    noImage={true}
                    text="Log In"
                  />
                  <PhLoginButton
                    event="register"
                    variant="contained"
                    noImage={true}
                    text="Register"
                  />
                </React.Fragment>
              )}
            </Grid>
          </Grid>
        </Container>
      </header>
      <main>
        {children}
      </main>
      <Footer/>
    </ThemeProviderLoaded>
  )
}

export default connect(
  state => ({ username: state.auth.nick }),
  {
    getBaseDevice,
    clearError
  }
)(MainLayout)
