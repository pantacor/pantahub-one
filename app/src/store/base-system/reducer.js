/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import * as Types from './types'
import { buildBasicReducers } from '../../lib/redux.helper'

const STATUS = {
  GET_DEVICE: {
    IN_PROGRESS: 'GET_DEVICE_IN_PROGRESS',
    FAILURE: 'GET_DEVICE_FAILURE',
    SUCCESS: 'GET_DEVICE_SUCCESS'
  }
}

export const initialState = {
}

const ActionMapper = {
  ...buildBasicReducers(
    STATUS.GET_DEVICE,
    Types,
    Types.BASE_SYSTEM_GET_DEVICE,
    undefined,
    undefined,
    (state = {}, action) => ({ ...state, [action.id]: action.payload })
  )
}

export default function reducer (state = initialState, action) {
  return ActionMapper[action.type] ? ActionMapper[action.type](state, action) : state
}
