/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'
import * as Service from '../../services/devices.service'
import { buildIndexedActions } from '../../lib/redux.helper'
import { GetSourceIdByModel } from '../../lib/devices.helper'
import { GetDeviceTrail } from '../devices/actions'
import { processService } from '../../lib/api.helper'
import { catchError } from '../general-errors/actions'
import { resolvePath } from '../../lib/utils.helper'

const getDeviceActions = buildIndexedActions(Types, Types.BASE_SYSTEM_GET_DEVICE)

export const getBaseDevice = (model) => async (dispatch, getState) => {
  dispatch(getDeviceActions.inProgress(model))
  const state = getState()
  const deviceData = resolvePath(state, `baseSystem.${model}`, {})
  let deviceID = (deviceData.id || '').split('-')[0]

  return processService(
    async () => {
      if (deviceID === '') {
        const deviceResp = await Service.GetDeviceByShortname(state.auth.token, GetSourceIdByModel(model))
        if (!deviceResp.ok) {
          return deviceResp
        }
        deviceID = deviceResp.json.id
      }

      return GetDeviceTrail(state.auth.token, deviceID, '')
    },
    (json) => {
      return dispatch(getDeviceActions.success(model, json.currentStep))
    },
    (error) => {
      return dispatch(catchError(error, getDeviceActions.failure.bind(null, model)))
    }
  )
}
