/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'
import * as Service from '../../services/devices.service'
import { buildBasicActions, buildIndexedActions } from '../../lib/redux.helper'
import { processService } from '../../lib/api.helper'
import {
  createInstalledState,
  createUninstalledState
} from './platformSetup'
import {
  SECURITY_OPTION_USER_META_KEY,
  HTTPD_USER_USER_META_KEY,
  HTTPD_PASS_USER_META_KEY,
  HTTPD_USER,
  HTTPD_PASS,
  CLOUDFARE_OPEN_URL
} from '../../lib/const'
import { catchError } from '../general-errors/actions'
import { resolvePath } from '../../lib/utils.helper'
import {
  AddDevice as AddDeviceToLocalStorage,
  RemoveDevice as RemoveDeviceFromLocalStorage
} from '../../lib/localdevices.helper'
import { GetDeviceModel, GetSourceIdByModel } from '../../lib/devices.helper'
import { GetPosibleUpdates } from '../../lib/state.helper'

const getDevicesActions = buildBasicActions(Types, Types.DEVICES_GET_ALL)
const getDeviceActions = buildIndexedActions(Types, Types.DEVICE_GET)
const getTrailActions = buildIndexedActions(Types, Types.TRAIL_GET)
const deviceSetMetadataActions = buildIndexedActions(Types, Types.DEVICE_SET_METADATA)
const devicePatchActions = buildIndexedActions(Types, Types.DEVICE_PATCH)
const devicePostStepActions = buildIndexedActions(Types, Types.DEVICE_POST_STEP)
export const deviceDeployActions = buildIndexedActions(Types, Types.DEVICE_DEPLOY)

// const currentStepStatuses = ['NEW', 'UPDATED', 'INPROGRESS', 'QUEUED', 'DONE', 'DOWNLOADING']

export const GetDeviceTrail = async (token, id, query, device) => {
  let deviceData = {
    steps: null,
    currentStep: null,
    activeStep: null
  }
  const trailsResponse = await Service.getDeviceTrails(token, id, query)
  if (!trailsResponse.ok) {
    return trailsResponse
  }

  deviceData.steps = trailsResponse.json.sort((a, b) => a.rev < b.rev)
  // get the latest applied revision
  deviceData.currentStep = findCurrentStep(deviceData.steps)
  if (device) {
    deviceData.activeStep = findActiveStep(device, deviceData.steps)
  }

  const response = {
    ...trailsResponse,
    json: deviceData
  }
  return response
}

const createNewStep = (deviceData, newState, commitMsg) => {
  const newRev = findLatestStep(deviceData.steps).rev + 1
  return {
    rev: newRev,
    'commit-msg': commitMsg,
    state: newState
  }
}

function findActiveStep (device, steps = []) {
  return steps.find((step) => {
    const activeRev = Number(device['device-meta']['pantavisor.revision'])
    return activeRev === step.rev
  }) || {}
}

function findLatestStep (steps = []) {
  return steps.reduce((acc, val) => val.rev > (acc.rev || -1) ? val : acc, {})
}

function findCurrentStep (steps = []) {
  return steps.reduce((acc, val) =>
    val.rev > (acc.rev || -1) // && currentStepStatuses.indexOf(val.progress.status) >= 0
      ? val
      : acc
  , {})
}

export const getDevices = (times = 0) => async (dispatch, getState) => {
  if (times > 0) {
    dispatch(getDevicesActions.inProgress())
  }
  const state = getState()

  return processService(
    async () => {
      const allDevicesResponse = await Service.getAllDevices(state.auth.token)
      if (!allDevicesResponse.ok || !allDevicesResponse.json || !Array.isArray(allDevicesResponse.json)) {
        return allDevicesResponse
      }
      const devices = allDevicesResponse.json.map((device) => ({
        device,
        loadingDevice: false,
        steps: null,
        currentStep: null,
        loadingTrail: false
      }))
      const getDevicesResponse = {
        ...allDevicesResponse,
        json: devices
      }
      return getDevicesResponse
    },
    (resp) => dispatch(getDevicesActions.success(resp)),
    (error) => dispatch(catchError(error, getDevicesActions.failure))
  )
}

export const getDevice = (id) => async (dispatch, getState) => {
  dispatch(getDeviceActions.inProgress(id))
  const state = getState()

  return processService(
    async () => {
      let deviceData = {
        device: null
      }
      const deviceResponse = await Service.GetDeviceByShortname(state.auth.token, id)
      deviceData.device = deviceResponse.json
      const response = {
        ...deviceResponse,
        json: deviceData
      }
      return response
    },
    (resp) => dispatch(getDeviceActions.success(id, resp)),
    (error) => dispatch(catchError(error, getDeviceActions.failure.bind(null, id)))
  )
}

export const getTrail = (id) => async (dispatch, getState) => {
  dispatch(getTrailActions.inProgress(id))
  const state = getState()
  const device = state.devices.list.items[id].device

  return processService(
    async () => {
      const trails = await GetDeviceTrail(state.auth.token, id, undefined, device)
      if (!trails.ok) {
        return trails
      }
      const comparation = GetPosibleUpdates(
        state.baseSystem,
        resolvePath(trails, 'json.activeStep.state', {}),
        device
      )
      trails.json.needUpdate = comparation.platformsWithUpdate.length > 0
      return trails
    },
    (resp) => dispatch(getTrailActions.success(id, resp)),
    (error) => dispatch(catchError(error, getTrailActions.failure.bind(null, id)))
  )
}

export const deviceSetMetadata = (id, type, metaData) => async (dispatch, getState) => {
  dispatch(deviceSetMetadataActions.inProgress(id))
  const state = getState()

  return processService(
    async () => {
      const deviceResponse = await Service.GetDeviceByShortname(state.auth.token, id)
      const device = deviceResponse.json
      const oldMeta = device[type] || {}
      const newMeta = { ...oldMeta, ...metaData }
      const setMetadataResponse = await Service.setDeviceMetadata(state.auth.token, id, newMeta, type)
      const deviceSetMetadataResponse = {
        ...setMetadataResponse,
        json: {
          ...device,
          [type]: setMetadataResponse.json
        }
      }
      return deviceSetMetadataResponse
    },
    (device) => dispatch(deviceSetMetadataActions.success(id, device)),
    (error) => dispatch(catchError(error, deviceSetMetadataActions.failure.bind(null, id)))
  )
}

export const devicePatch = (id, payload) => async (dispatch, getState) => {
  dispatch(devicePatchActions.inProgress(id))
  const state = getState()

  return processService(
    async () => {
      await Service.patchDevice(state.auth.token, id, payload)
      // currently user-meta is corrupted in patchDevice response, so we need to get the updated device:
      const deviceResponse = await Service.GetDeviceByShortname(state.auth.token, id)
      return deviceResponse
    },
    (device) => dispatch(devicePatchActions.success(id, device)),
    (error) => dispatch(catchError(error, devicePatchActions.failure.bind(null, id)))
  )
}

export const devicePostStep = (id, newStep) => async (dispatch, getState) => {
  dispatch(devicePostStepActions.inProgress(id))
  const state = getState()

  return processService(
    async () => {
      const postTrailResponse = await Service.postDeviceTrailStep(state.auth.token, id, newStep)
      if (!postTrailResponse.ok) {
        throw new Error(`Can't publish the revision ${newStep.rev}, maybe there was post another revision`)
      }
      const device = state.devices.list.items[id].device
      const response = await GetDeviceTrail(state.auth.token, id, undefined, device)
      return response
    },
    (deviceData) => dispatch(devicePostStepActions.success(id, deviceData)),
    (error) => dispatch(catchError(error, devicePostStepActions.failure.bind(null, id)))
  )
}

export const devicePostState = (id, newState, commitMsg) => async (dispatch, getState) => {
  dispatch(devicePostStepActions.inProgress(id))
  const state = getState()

  return processService(
    async () => {
      const destinationDeviceData = state.devices.list.items[id]
      const destinationDeviceState = resolvePath(destinationDeviceData, 'activeStep.state', null)

      if (destinationDeviceState) {
        const newStep = createNewStep(destinationDeviceData, newState, commitMsg)

        const postTrailResponse = await Service.postDeviceTrailStep(state.auth.token, id, newStep)
        if (!postTrailResponse.ok) {
          throw new Error(`Can't publish the revision ${newStep.rev}, maybe there was post another revision`)
        }

        const device = state.devices.list.items[id].device
        const response = await GetDeviceTrail(state.auth.token, id, undefined, device)
        return response
      } else {
        throw new Error('Required data not found')
      }
    },
    (deviceData) => dispatch(devicePostStepActions.success(id, deviceData)),
    (error) => dispatch(catchError(error, devicePostStepActions.failure.bind(null, id)))
  )
}

export const installPlatform = (id) => async (dispatch, getState) => {
  dispatch(devicePostStepActions.inProgress(id))
  const state = getState()
  const model = GetDeviceModel(resolvePath(state.devices.list.items[id], 'device', {}))

  return processService(
    async () => {
      const sourceDevice = await Service.GetDeviceByShortname(state.auth.token, GetSourceIdByModel(model))
      if (!sourceDevice.ok) {
        return sourceDevice
      }

      const sourceDeviceResponse = await GetDeviceTrail(state.auth.token, sourceDevice.json.id)
      const destinationDeviceData = state.devices.list.items[id]
      const sourceDeviceState = resolvePath(sourceDeviceResponse, 'json.currentStep.state', null)
      const destinationDeviceState = resolvePath(destinationDeviceData, 'activeStep.state', null)

      if (sourceDeviceState && destinationDeviceState) {
        // Create the new state
        const newState = createInstalledState(sourceDeviceState, destinationDeviceState)
        // Create the new step to post
        const newStep = createNewStep(destinationDeviceData, newState, 'Cloudflare platfom installed from One App.')

        const postTrailResponse = await Service.postDeviceTrailStep(state.auth.token, id, newStep)
        if (!postTrailResponse.ok) {
          throw new Error(`Can't publish the revision ${newStep.rev}, maybe there was post another revision`)
        }

        const response = await GetDeviceTrail(state.auth.token, id, undefined, destinationDeviceData.device)
        return response
      } else {
        throw new Error('Required data not found')
      }
    },
    (deviceData) => {
      AddDeviceToLocalStorage(id, resolvePath(getState(), `devices.list.items.${id}.device`))
      dispatch(devicePostStepActions.success(id, deviceData))
      let meta = {
        [HTTPD_USER_USER_META_KEY]: HTTPD_USER,
        [HTTPD_PASS_USER_META_KEY]: HTTPD_PASS,
        [SECURITY_OPTION_USER_META_KEY]: CLOUDFARE_OPEN_URL
      }
      return dispatch(deviceSetMetadata(id, 'user-meta', meta))
    },
    (error) => {
      return dispatch(catchError(error, devicePostStepActions.failure.bind(null, id)))
    }
  )
}

export const uninstallPlatform = (id) => async (dispatch, getState) => {
  dispatch(devicePostStepActions.inProgress(id))
  const state = getState()

  return processService(
    async () => {
      const destinationDeviceData = state.devices.list.items[id]
      const destinationDeviceState = resolvePath(destinationDeviceData, 'activeStep.state', null)

      if (destinationDeviceState) {
        // Remove cloudflare keys from previous state. Remove wifi hotspot configuration
        let newState = createUninstalledState(destinationDeviceState)
        // Create the new step to post
        const newStep = createNewStep(destinationDeviceData, newState, 'Cloudflare platform uninstalled from One App.')
        /* const postDeviceTrailStepResponse = */ await Service.postDeviceTrailStep(state.auth.token, id, newStep)
        const response = await GetDeviceTrail(state.auth.token, id, undefined, destinationDeviceData.device)
        return response
      } else {
        throw new Error('Required data not found')
      }
    },
    (deviceData) => {
      RemoveDeviceFromLocalStorage(id)
      dispatch(devicePostStepActions.success(id, deviceData))
      let meta = {
        [HTTPD_USER_USER_META_KEY]: undefined,
        [HTTPD_PASS_USER_META_KEY]: undefined,
        [SECURITY_OPTION_USER_META_KEY]: undefined
      }
      return dispatch(deviceSetMetadata(id, 'user-meta', meta))
    },
    (error) => dispatch(catchError(error, devicePatchActions.failure.bind(null, id)))
  )
}
