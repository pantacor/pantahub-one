/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { resolvePath } from '../../lib/utils.helper'
import orderBy from 'lodash.orderby'
import {
  SOURCE_DEVICE_ID,
  AWCONNECT_DOCKER_NAME,
  AVAHI_DOCKER_NAME,
  CLOUDFLARE_DOCKER_NAME,
  DEFAULT_DEVICE_DNS_CONF_VALUE,
  DEFAULT_DEVICE_HOSTNAME_VALUE,
  DEFAULT_WIFI_SSID,
  DEFAULT_WIFI_PSK,
  SUPPORTED_ARCH
} from '../../lib/const'

export const RADIO_5GHZ = '5'
export const RADIO_24GHZ = '2.4'
export const SECURITY_WPA1 = 'wpa1'
export const SECURITY_WPA2 = 'wpa2'

const getConfigKey = (platformDir, path) => {
  return `_config/${platformDir}${path}`
}

export const getDeviceHostnameKey = (state) => {
  const avahiDir = findAvahiDir(state)
  return (avahiDir ? getConfigKey(avahiDir, `/etc/hostname`) : undefined)
}

export const getDeviceDnsConfKey = (state) => {
  const awConnectDir = findAWConnectDir(state)
  return (awConnectDir ? getConfigKey(awConnectDir, `/etc/NetworkManager/dnsmasq-shared.d/device-dns.conf`) : undefined)
}

export const getWifiHotspotJsonKey = (state) => {
  const awConnectDir = findAWConnectDir(state)
  return (awConnectDir ? getConfigKey(awConnectDir, `/etc/wifi-connect/conf.d/WifiHotspot.json`) : undefined)
}

export const mapWifiHotpostToState = (wifiHotSpot = {}) => {
  return {
    ssid: wifiHotSpot.ssid || '',
    psk: wifiHotSpot.psk || '',
    security: wifiHotSpot.group && wifiHotSpot.pairwise && wifiHotSpot.proto ? SECURITY_WPA2 : SECURITY_WPA1,
    radio: wifiHotSpot.band ? RADIO_5GHZ : RADIO_24GHZ
  }
}

export const mapWifiStateToWifiHotspot = (wifiState = {}) => {
  const state = {}
  if (wifiState.radio === RADIO_5GHZ) {
    state['band'] = 'a'
  }

  if (wifiState.security === SECURITY_WPA2) {
    state['group'] = 'ccmp'
    state['pairwise'] = 'ccmp'
    state['proto'] = 'rsn'
  }

  state['psk'] = wifiState.psk
  state['ssid'] = wifiState.ssid

  return state
}

export const getWifiHotspotJsonEntry = (state) => {
  const wifiHotspotJsonStateKey = getWifiHotspotJsonKey(state)
  return wifiHotspotJsonStateKey
    ? [ wifiHotspotJsonStateKey, mapWifiHotpostToState(state[wifiHotspotJsonStateKey]) ]
    : undefined
}

const deviceText = (device) => (device ? device.nick || device.id || '' : '')

export function orderDevices (devices) {
  return orderBy(
    devices,
    [
      'isOnline',
      'group',
      'device.nick'
    ],
    [
      'desc',
      'desc',
      'asc'
    ]
  )
}

export function compareDevices (deviceDataA, deviceDataB) {
  return (deviceDataA.group !== deviceDataB.group
    ? deviceDataA.group - deviceDataB.group
    : deviceText(deviceDataA.device).localeCompare(deviceText(deviceDataB.device)))
}

export function platformIsSupported (deviceData) {
  const arch = resolvePath(deviceData, 'device.device-meta', {})['pantavisor.arch']
  const deviceId = resolvePath(deviceData, 'device.id')
  const modelSupported = true

  const archSupported = arch
    ? SUPPORTED_ARCH.some(item => arch.toLowerCase() === item)
    : false

  return deviceId !== SOURCE_DEVICE_ID && archSupported && modelSupported
}

const srcJsonRegex = new RegExp('^([^/]*)/src.json$')

export const findPlatformEntries = (state, dir) => {
  return (state && dir ? Object.entries(state).filter(([key, value]) => key.indexOf(dir) === 0) : [])
}

export const findPlatformDir = (state, dockerName) => {
  return Object.entries(state).reduce((acc, [key, value]) => {
    if (acc) {
      return acc
    } else {
      const match = srcJsonRegex.exec(key)
      if (match) {
        return (value.docker_name === dockerName ? match[1] : undefined)
      } else {
        return undefined
      }
    }
  }, undefined)
}

export const findCloudflareDir = (state) => {
  return findPlatformDir(state, CLOUDFLARE_DOCKER_NAME)
}

export const findAWConnectDir = (state) => {
  return findPlatformDir(state, AWCONNECT_DOCKER_NAME)
}

export const findAvahiDir = (state) => {
  return findPlatformDir(state, AVAHI_DOCKER_NAME)
}

export const platformIsInstalled = (deviceData) => {
  return true
  // return !!resolvePath(deviceData, 'device.user-meta', {})['cloudflared.upstream']
}

const setWifiConnectPersistence = (state, dir, newMode) => {
  const runJson = (state ? state[dir + '/run.json'] : null)
  const storage = (runJson ? runJson.storage : null)
  const docker = (storage ? storage['docker--etc-NetworkManager-system-connections'] : null)
  if (docker) {
    // docker.persistence = newMode
  }
}

export const sortState = (state) => {
  let newState = {}
  Object.entries(state).sort(([keyA, valueA], [keyB, valueB]) => keyA.localeCompare(keyB)).forEach(([key, value]) => { newState[key] = value })
  return newState
}

export const createInstalledState = (sourceDeviceState, destinationDeviceState) => {
  // Copy cloudflare and awconnect entries from source device state to destination
  const cloudflareDir = findCloudflareDir(sourceDeviceState)
  const awConnectDir = findAWConnectDir(sourceDeviceState)
  const awConnectDirDst = findAWConnectDir(sourceDeviceState)
  const cloudflareEntries = findPlatformEntries(sourceDeviceState, cloudflareDir)

  // If awConnect is not installed, then install it
  const awConnectEntries = findPlatformEntries(sourceDeviceState, awConnectDir)

  let newState = {}
  let aux = [
    ...(Object.entries(destinationDeviceState)),
    ...cloudflareEntries,
    ...awConnectEntries
  ]
  aux.forEach(([key, value]) => { newState[key] = value })

  // Create install configuration
  const deviceDnsConfKey = getDeviceDnsConfKey(newState)
  const deviceHostnameKey = getDeviceHostnameKey(newState)
  const wifiHotspotKey = getWifiHotspotJsonKey(newState)
  newState = {
    ...newState,
    [deviceDnsConfKey]: DEFAULT_DEVICE_DNS_CONF_VALUE,
    [deviceHostnameKey]: DEFAULT_DEVICE_HOSTNAME_VALUE,
    [wifiHotspotKey]: {
      ssid: DEFAULT_WIFI_SSID,
      psk: DEFAULT_WIFI_PSK
    }
  }
  // Set persistence mode
  setWifiConnectPersistence(newState, awConnectDirDst, 'boot')
  return sortState(newState)
}

export const createUninstalledState = (state) => {
  // Remove cloudflare keys from previous state. Remove wifi hotspot configuration
  let newState = {}
  const cloudflareDir = findCloudflareDir(state)
  const awConnectDir = findAWConnectDir(state)
  const cloudflareEntries = findPlatformEntries(state, cloudflareDir)
  let aux = [
    ...(Object.entries(state))
      .filter(([key, value]) => (cloudflareEntries.find(([ceKey, ceValue]) => key === ceKey) === undefined)) // Remove cloudflare platform entries
  ]
  aux.forEach(([key, value]) => { newState[key] = value })
  // Remove install configuration
  const deviceDnsConfKey = getDeviceDnsConfKey(state)
  const wifiHotspotJsonKey = getWifiHotspotJsonKey(state)
  delete newState[deviceDnsConfKey]
  delete newState[wifiHotspotJsonKey]
  // Set persistence mode
  setWifiConnectPersistence(newState, awConnectDir, 'permanent')
  return sortState(newState)
}
