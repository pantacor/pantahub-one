/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const DEVICES_GET_ALL = 'DEVICES_GET_ALL'
export const DEVICES_GET_ALL_INPROGRESS = 'devices/get/all/inprogress'
export const DEVICES_GET_ALL_SUCCESS = 'devices/get/all/success'
export const DEVICES_GET_ALL_FAILURE = 'devices/get/all/failure'

export const DEVICE_GET = 'DEVICE_GET'
export const DEVICE_GET_INPROGRESS = 'device/get/inprogress'
export const DEVICE_GET_SUCCESS = 'device/get/success'
export const DEVICE_GET_FAILURE = 'device/get/failure'

export const TRAIL_GET = 'TRAIL_GET'
export const TRAIL_GET_INPROGRESS = 'trail/get/inprogress'
export const TRAIL_GET_SUCCESS = 'trail/get/success'
export const TRAIL_GET_FAILURE = 'trail/get/failure'

export const DEVICE_SET_METADATA = 'DEVICE_SET_METADATA'
export const DEVICE_SET_METADATA_INPROGRESS = 'device/set-metadata/inprogress'
export const DEVICE_SET_METADATA_SUCCESS = 'device/set-metadata/success'
export const DEVICE_SET_METADATA_FAILURE = 'device/set-metadata/failure'

export const DEVICE_PATCH = 'DEVICE_PATCH'
export const DEVICE_PATCH_INPROGRESS = 'device/patch/inprogress'
export const DEVICE_PATCH_SUCCESS = 'device/patch/success'
export const DEVICE_PATCH_FAILURE = 'device/patch/failure'

export const DEVICE_POST_STEP = 'DEVICE_POST_STEP'
export const DEVICE_POST_STEP_INPROGRESS = 'device/post-step/inprogress'
export const DEVICE_POST_STEP_SUCCESS = 'device/post-step/success'
export const DEVICE_POST_STEP_FAILURE = 'device/post-step/failure'

export const DEVICE_DEPLOY = 'DEVICE_DEPLOY'
export const DEVICE_DEPLOY_INPROGRESS = 'device/deploy/inprogress'
export const DEVICE_DEPLOY_SUCCESS = 'device/deploy/success'
export const DEVICE_DEPLOY_FAILURE = 'device/deploy/failure'
