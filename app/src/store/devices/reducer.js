/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'
import dayjs from 'dayjs'

import { buildBasicReducers } from '../../lib/redux.helper'
import { platformIsInstalled, platformIsSupported } from './platformSetup'

export const STATUS = {
  IN_PROGRESS: 'DEVICES_INPROGRESS',
  FAILURE: 'DEVICES_FAILURE',
  SUCCESS: 'DEVICES_SUCCESS',
  WAITING_DEPLOY: 'WAITING_DEPLOY',
  GET_DEVICE: {
    IN_PROGRESS: 'GET_DEVICE_IN_PROGRESS',
    FAILURE: 'GET_DEVICE_FAILURE',
    SUCCESS: 'GET_DEVICE_SUCCESS'
  },
  GET_STEP: {
    IN_PROGRESS: 'GET_STEP_IN_PROGRESS',
    FAILURE: 'GET_STEP_FAILURE',
    SUCCESS: 'GET_STEP_SUCCESS'
  },
  CLONING: {
    IN_PROGRESS: 'CLONING_IN_PROGRESS',
    FAILURE: 'CLONING_FAILURE',
    SUCCESS: 'CLONING_SUCCESS'
  },
  DEPLOY: {
    IN_PROGRESS: 'DEPLOY_IN_PROGRESS',
    FAILURE: 'DEPLOY_FAILURE',
    SUCCESS: 'DEPLOY_SUCCESS'
  },
  META: {
    IN_PROGRESS: 'META_IN_PROGRESS',
    FAILURE: 'META_FAILURE',
    SUCCESS: 'META_SUCCESS'
  }
}

export const initialState = {
  list: {
    items: {},
    total: 0,
    unsupportedDevicesCount: 0
  },
  status: null,
  clone: null,
  source: null,
  destination: null,
  selectedRevision: null,
  cloneUserMeta: false,
  formErrors: {}
}

const mergeDevicesListState = (state, action, successPrefix) => {
  const result = {
    ...state[successPrefix]
  }
  result.unsupportedDevicesCount = 0
  result.total = action.payload.length

  const payloadIds = action.payload.reduce((acc, deviceData) =>
    ({ ...acc, [deviceData.device.id]: true }), {}
  )

  // Remove deleted devices
  Object.keys(result.items).forEach((key) => {
    if (!payloadIds[key]) {
      delete result.items[key]
    }
  })

  result.items = action.payload
    .reduce((acc, deviceData, index) => {
      const oldData = acc[deviceData.device.id] || { device: {}, activeStep: null, currentStep: null }
      const isOnline = dayjs().diff(dayjs(deviceData.device['time-modified']), 'minute', true) < 1
      deviceData = {
        ...oldData,
        isOnline: isOnline,
        device: {
          ...oldData.device,
          ...deviceData.device
        }
      }
      const platformInstalled = platformIsInstalled(deviceData)
      const platformSupported = platformIsSupported(deviceData)

      if (platformInstalled || platformSupported) {
        acc[deviceData.device.id] = {
          ...deviceData,
          group: (platformInstalled ? 2 : (platformSupported ? 1 : 0)),
          platformInstalled,
          platformSupported
        }
      }

      if (!platformSupported) {
        result.unsupportedDevicesCount++
      }

      return acc
    }, result.items || {})

  return result
}

const ActionMapper = {
  ...buildBasicReducers(STATUS, Types, Types.DEVICES_GET_ALL, 'list', undefined, mergeDevicesListState),
  ...buildBasicReducers(STATUS.DEPLOY, Types, Types.DEVICE_DEPLOY),
  [Types.DEVICE_GET_INPROGRESS]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingDevice: true,
          error: null
        }
      }
    }
  }),
  [Types.DEVICE_GET_SUCCESS]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          ...action.payload,
          loadingDevice: false
        }
      }
    }
  }),
  [Types.DEVICE_GET_FAILURE]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingDevice: false,
          error: action.error
        }
      }
    }
  }),

  [Types.DEVICE_SET_METADATA_INPROGRESS]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingDevice: true,
          error: null,
          status: STATUS.META.IN_PROGRESS
        }
      }
    }
  }),
  [Types.DEVICE_SET_METADATA_SUCCESS]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingDevice: false,
          device: action.payload,
          status: STATUS.META.SUCCESS
        }
      }
    }
  }),
  [Types.DEVICE_SET_METADATA_FAILURE]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingDevice: false,
          error: action.error,
          status: STATUS.META.FAILURE
        }
      }
    }
  }),

  [Types.DEVICE_PATCH_INPROGRESS]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingDevice: true,
          error: null,
          status: STATUS.DEPLOY.IN_PROGRESS
        }
      }
    }
  }),
  [Types.DEVICE_PATCH_SUCCESS]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingDevice: false,
          device: action.payload,
          status: STATUS.DEPLOY.SUCCESS
        }
      }
    }
  }),
  [Types.DEVICE_PATCH_FAILURE]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingDevice: false,
          error: action.error,
          status: STATUS.DEPLOY.FAILURE
        }
      }
    }
  }),
  [Types.DEVICE_POST_STEP_INPROGRESS]: (state, action) => ({
    ...state,
    status: STATUS.WAITING_DEPLOY,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingTrail: true,
          loadingDevice: true,
          status: STATUS.DEPLOY.IN_PROGRESS,
          error: null
        }
      }
    }
  }),
  [Types.DEVICE_POST_STEP_SUCCESS]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          ...action.payload,
          loadingTrail: false,
          status: STATUS.DEPLOY.SUCCESS,
          loadingDevice: false
        }
      }
    }
  }),
  [Types.DEVICE_POST_STEP_FAILURE]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingTrail: false,
          loadingDevice: false,
          status: STATUS.DEPLOY.FAILURE,
          error: action.error
        }
      }
    }
  }),
  [Types.TRAIL_GET_INPROGRESS]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingTrail: true,
          error: null
        }
      }
    }
  }),
  [Types.TRAIL_GET_SUCCESS]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          ...action.payload,
          loadingTrail: false
        }
      }
    }
  }),
  [Types.TRAIL_GET_FAILURE]: (state, action) => ({
    ...state,
    list: {
      ...state.list,
      items: {
        ...state.list.items,
        [action.id]: {
          ...state.list.items[action.id],
          loadingTrail: false,
          error: action.error
        }
      }
    }
  })
}

export default function reducer (state = initialState, action) {
  return ActionMapper[action.type] ? ActionMapper[action.type](state, action) : state
}
