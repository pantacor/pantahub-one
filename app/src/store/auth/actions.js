/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* eslint-disable no-restricted-globals */

import * as Types from './types'
import { buildBasicActions } from '../../lib/redux.helper'
import { processService } from '../../lib/api.helper'
import { catchError } from '../general-errors/actions'
import { goToAuthorization, removeToken, getTokenFromImplicit, getUserData, setToken, getAnonSession } from '../../services/auth.service'
import { getFactoryToken } from '../../services/devices.service'

import {
  SECURITY_OPTION_USER_META_KEY,
  HTTPD_USER_USER_META_KEY,
  HTTPD_PASS_USER_META_KEY,
  HTTPD_USER,
  HTTPD_PASS,
  CLOUDFARE_KIDS_URL
} from '../../lib/const'

const IMAGE_CHANNEL = process.env.REACT_APP_SOURCE_IMAGE_CHANNEL || 'release-candidate'
const IMAGER_URL = 'https://images.apps.pantahub.com/get'
const LOCAL_DEVICE_OAUTH_URL = (deviceIp = 'one.local') =>
  `http://${HTTPD_USER}:${HTTPD_PASS}@${deviceIp}:8280/cgi-bin/oauth2`

const getConfig = (factoryToken) => {
  const host = new URL(process.env.REACT_APP_PH_API_URL || 'https://api.pantahub.com').hostname
  return window.btoa(`configargs=ph_factory.autotok=${factoryToken} ph_creds.host=${host} ph_updater.commit.delay=20\0`)
}

const getUserActions = buildBasicActions(Types, Types.AUTH_USER_GET)

export const Login = (url) => {
  goToAuthorization(url)
  return {
    type: Types.AUTH_CLEAR_TOKEN
  }
}

export const LoginWith = (where, deviceIp) => {
  return Login(where === 'local' ? LOCAL_DEVICE_OAUTH_URL(deviceIp) : null)
}

export const Logout = () => {
  removeToken()
  return {
    type: Types.AUTH_CLEAR_TOKEN
  }
}

export const SaveToken = (hash) => {
  const token = getTokenFromImplicit(hash)
  setToken(token.access_token)
  return SetToken(token.access_token)
}

export const LoginAnonimusly = () => async (dispatch) => {
  const token = await getAnonSession()
  if (token) {
    dispatch(SetToken(token))
  }
}

export const DownloadImageWithAuth = (device) => async (dispatch, getState) => {
  let token = getState().auth.token
  try {
    if (!token) {
      token = await getAnonSession()
      if (token) {
        dispatch(SetToken(token))
      }
    }
    let userMeta = {
      'default-user-meta': {
        [HTTPD_USER_USER_META_KEY]: HTTPD_USER,
        [HTTPD_PASS_USER_META_KEY]: HTTPD_PASS,
        [SECURITY_OPTION_USER_META_KEY]: CLOUDFARE_KIDS_URL,
        'dns-extra.conf': '# Games Filters\naddress=/epicgames.com/0.0.0.0'
      }
    }
    const factoryToken = await getFactoryToken(token, userMeta)
    const config = getConfig(factoryToken.json.token)
    const downloadURL = `${IMAGER_URL}?device=${device}&config=${config}&channel=${IMAGE_CHANNEL}&project=one`
    window.open(downloadURL, 'downloadWindow', 'noopener,noreferrer')
  } catch (e) {
    console.info(e)
  }
}

export const SetToken = (token) => ({
  type: Types.AUTH_SAVE_TOKEN,
  payload: token
})

export const SaveTokenAndRedirect = (hash) => (dispatch) => {
  dispatch(SaveToken(hash))
  window.location.hash = ''
}

export const GetUser = () => async (dispatch, getState) => {
  dispatch(getUserActions.inProgress())
  const state = getState()

  if (!state.auth.token) {
    return null
  }

  return processService(
    getUserData.bind(null, state.auth.token),
    (resp) => dispatch(getUserActions.success(resp)),
    (error) => dispatch(catchError(error, getUserActions.failure))
  )
}
