/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import * as Types from './types'
import jwtDecode from 'jwt-decode'
import { JWT_NAME } from '../../services/auth.service'
import { buildBasicReducers } from '../../lib/redux.helper'
import { Track } from '../../lib/analytics.helper'

const authInfo = window.localStorage.getItem(JWT_NAME)
const [token, username] = (authInfo || '').split('|')
const decodeJwt = (jwt = '') => {
  try {
    return jwtDecode(jwt)
  } catch (e) {
    return {}
  }
}

export const STATUS = {
  IN_PROGRESS: 'AUTH_INPROGRESS',
  SUCCESS: 'AUTH_SUCCESS',
  FAILURE: 'AUTH_FAILURE'
}

const initialState = {
  username: username || '',
  token: token || null,
  exp: null,
  id: '',
  nick: '',
  orig_iat: null,
  prn: '',
  roles: '',
  type: '',
  gettingToken: false,
  getTokenError: null,
  status: null,
  ...decodeJwt(token)
}

const mergeState = (state, action, successPrefix) => ({
  ...state,
  ...action.payload
})

const ActionMapper = {
  ...buildBasicReducers(STATUS, Types, Types.AUTH_USER_GET, undefined, undefined, mergeState),
  [Types.AUTH_SAVE_TOKEN]: (state, action) => {
    const decoded = decodeJwt(action.payload)

    Track('logged-in', { username: decoded.nick })
    return {
      ...state,
      ...decoded,
      status: STATUS.SUCCESS,
      token: action.payload
    }
  },
  [Types.AUTH_CLEAR_TOKEN]: (state, action) => ({ ...initialState, token: null })
}

export default function authReducer (state = initialState, action) {
  return ActionMapper[action.type] ? ActionMapper[action.type](state, action) : state
}
