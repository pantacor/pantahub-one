/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { getDeviceTrails, GetDeviceByShortname } from './devices.service'
import { GetAppsNamesFromState } from '../lib/markeplace.helper'

const getDeviceWithToken = token => source => GetDeviceByShortname(token, source)

export const GetAppsFromSources = async (token, sources = []) => {
  const devicesResponse = await Promise.all(sources.map(getDeviceWithToken(token)))

  const trailsResponse = await Promise.all(devicesResponse.reduce((acc, resp) => {
    if (resp.ok) {
      acc.push(getDeviceTrails(token, resp.json.id))
    }

    return acc
  }, []))

  const apps = trailsResponse.reduce((acc, trailResp) => {
    if (trailResp.ok && trailResp.json && trailResp.json.length) {
      const currentRev = trailResp.json[trailResp.json.length - 1]
      acc = [
        ...acc,
        ...GetAppsNamesFromState(currentRev.state, currentRev.device.replace(`prn:::devices:/`, ``))
      ]
    }
    return acc
  }, [])

  return apps
}
