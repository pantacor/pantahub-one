/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/* eslint-disable no-restricted-globals */
import {
  _getJSON,
  _postJSON,
  API_URL
} from '../lib/api.helper'

const AUTH_STATUS_URL = `${API_URL}/auth/auth_status`
const ANON_AUTH_URL = `${process.env.REACT_APP_PH_API_URL}/auth/sessions`
const OAUTH_AUTH_URL = `${process.env.REACT_APP_PH_URL}/oauth2/authorize`
const OAUTH_CLIENT_ID = process.env.REACT_APP_OAUTH_CLIENT_ID
const OAUTH_DEFAULT_SCOPE = process.env.REACT_APP_OAUTH_DEFAULT_SCOPE
const redirectToDefault = () => `${window.location.href}`

export const JWT_NAME = '_ph_token'

export function getTokenFromImplicit (hash) {
  return (hash.match(/([^#=&]+)(=([^&]*))?/g) || []).reduce((acc, val) => {
    const [key, value] = val.split('=')
    acc[key] = value
    return acc
  }, {})
}

export const OAuthUrl = (
  url,
  scope = OAUTH_DEFAULT_SCOPE,
  redirectTo,
  responseType = 'token',
  clientId = OAUTH_CLIENT_ID
) => {
  url = !url ? OAUTH_AUTH_URL : url
  redirectTo = !redirectTo ? redirectToDefault() : redirectTo
  return encodeURI(`${url}?client_id=${clientId}&scope=${scope}&redirect_uri=${redirectTo}&response_type=${responseType}`)
}

export const goToAuthorization = (url) => {
  window.localStorage.removeItem(JWT_NAME)
  window.location.href = OAuthUrl(url)
}

export async function getAnonSession () {
  const r = await _postJSON(ANON_AUTH_URL, '', '')
  setToken(r.json['token'])
  return r.json['token']
}

export const removeToken = () =>
  window.localStorage.removeItem(JWT_NAME)

export const setToken = (token) =>
  window.localStorage.setItem(JWT_NAME, token)

export async function getUserData (token) {
  return _getJSON(AUTH_STATUS_URL, token)
}
