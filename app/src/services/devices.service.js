/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import {
  _getJSON,
  _postJSON,
  _putJSON,
  _patchJSON,
  API_URL
} from '../lib/api.helper'

const DEVS_URL = `${API_URL}/devices`
const SUMMARY_URL = `${API_URL}/trails/summary`

const devicesUrl = () =>
  `${DEVS_URL}/`

const deviceUrl = (deviceId) =>
  `${DEVS_URL}/${deviceId}`

const devicesTokenUrl = () =>
  `${DEVS_URL}/tokens`

const trailsUrl = (deviceId) =>
  `${API_URL}/trails/${deviceId}/steps`

const trailStepRevisionUrl = (deviceId, rev) =>
  `${API_URL}/trails/${deviceId}/steps/${rev}`

const deviceTrailsSummaryUrl = (id) =>
  `${API_URL}/trails/${id}/summary`

export const getDeviceTrailsSummary = async (token, id) =>
  _getJSON(deviceTrailsSummaryUrl(id), token)

export const getAllTrailsSummaries = async (token) =>
  _getJSON(SUMMARY_URL, token)

export const postDeviceTrailStep = async (token, id, payload) =>
  _postJSON(trailsUrl(id), token, payload)

export const getFactoryToken = async (token, body) =>
  _postJSON(devicesTokenUrl(), token, body)

export const setDeviceMetadata = async (token, id, meta, type = 'user-meta') =>
  _putJSON(`${DEVS_URL}/${id}/${type}`, token, meta)

export const getAllDevices = async (token) =>
  _getJSON(devicesUrl(), token)

export const getDevice = async (token, id) =>
  _getJSON(deviceUrl(id), token)

export const getDeviceTrails = async (token, id, query = '?progress.status={%22$ne%22:%22%22}') =>
  _getJSON(trailsUrl(id) + query, token)

export const getDeviceTrailStep = async (token, id, rev) =>
  _getJSON(trailStepRevisionUrl(id, rev), token)

export const patchDevice = async (token, id, payload) =>
  _patchJSON(`${DEVS_URL}/${id}`, token, payload)

export const getDeviceByNick = async (token, devicenick) =>
  _getJSON(`${DEVS_URL}/np/${devicenick}`, token)

export const GetDeviceByShortname = (token, source) => {
  const getDeviceService = source.indexOf('/') >= 0 ? getDeviceByNick : getDevice

  let deviceID = source
  if (deviceID.indexOf('http') >= 0) {
    const sourceUrl = new URL(deviceID)
    // eslint-disable-next-line no-unused-vars
    const [_, username, device] = sourceUrl.pathname.split('/')
    deviceID = `${username}/${device}`
  }

  return getDeviceService(token, deviceID)
}
