/*
 * Copyright (c) 2020 Pantacor Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the 'Software'), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { useRef, useEffect, useCallback } from 'react'

const REFRESH_RATE = Number(process.env.REACT_APP_REFRESH_TIME || '5000')

export function useInterval (callback, autostart = true, delay = REFRESH_RATE) {
  const savedCallback = useRef(null)
  const intervalId = useRef(undefined)
  const polling = useRef(0)

  const tick = () => {
    if (savedCallback.current) {
      savedCallback.current(polling.current)
      polling.current++
    }
  }

  const removeInterval = () => {
    clearInterval(intervalId.current)
    intervalId.current = undefined
  }

  const startInterval = useCallback(() => {
    if (intervalId.current && delay === null) {
      removeInterval()
    }

    if (delay !== null && !intervalId.current) {
      intervalId.current = setInterval(tick, delay)
      tick()
    }

    return () => {
      removeInterval()
    }
  }, [delay])

  useEffect(() => {
    savedCallback.current = callback
  })

  useEffect(() => {
    if (autostart) {
      return startInterval()
    }
  }, [autostart, startInterval])

  return {
    startInterval,
    removeInterval,
    intervalId,
    tick
  }
}
