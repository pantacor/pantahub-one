Pantahub One
============

## About this project

Pantahub ONE is a web application to control and configurate an pantahub enabled device as a Wi-Fi hotspot .

Using Cloudflare’s 1.1.1.1 for Families and Pantahub ONE anyone can setup a Wi-Fi hotspot with configurable parental controls and filters to make sure your kids are not consuming content that wasn’t meant for them.

### Getting Started

To get started you can go to https://one.apps.pantahub.com and sign up for a Pantahub account, then follow the simple instructions to set up your Raspberry Pi. Once finished, you can connect your children’s devices to the new network, making sure they are protected even if you are not there to look over their shoulder all the time.
