FROM node:14.18.1-bullseye-slim as builder

ARG NS=development

RUN apt-get update && apt-get install -y git python3

WORKDIR /builder

COPY .git/ /builder/.git
COPY app/ /builder/

RUN set -a && REACT_APP_REVISION=$(git --work-tree=../ describe --always --tags)-$NS && . /builder/env.$NS && set +a && yarn && yarn build

FROM nginx

COPY --from=builder /builder/build /usr/share/nginx/html
COPY --from=builder /builder/build /usr/share/nginx/html/app/
COPY nginx.conf /etc/nginx/conf.d/default.conf
